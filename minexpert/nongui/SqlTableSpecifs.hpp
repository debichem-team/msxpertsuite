/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


// Qt includes
#include <QString>
#include <QList>


// Local includes
#include <minexpert/nongui/SqlTableFieldSpecifs.hpp>

namespace msXpSmineXpert
{


//! The SqlTableSpecifs class provides a configuration for a SQL database table.
/*!

  When creating a new mass spectrometry data SQLite3-based formatted file, the
  tables need to be created as a very first step. This class provides a
  configuration mechanism that is used to configure a database table. In
  particular, its m_sqlTableFieldSpecifList member is a list of all the
  SqlTableFieldSpecifs instances required to configure the various fields of
  the table.

*/
class SqlTableSpecifs
{
  private:
  //! Name of the table.
  QString m_tableName;

  //! List of all the field specifications.
  QList<SqlTableFieldSpecifs *> m_sqlTableFieldSpecifList;

  //! Comment to the table.
  QString m_comment;

  //! Command line.
  QString m_sqlStatementSeed;

  public:
  SqlTableSpecifs(QString tableName        = QString(),
                  QString comment          = QString(),
                  QString sqlStatementSeed = QString());
  SqlTableSpecifs(const QString &tableName);
  virtual ~SqlTableSpecifs();

  void clearTableFieldSpecif();
  void reset();

  void setTableName(QString tableName);
  QString tableName() const;
  void setComment(QString comment);
  QString comment() const;
  void setSqlStatementSeed(QString sqlStatementSeed);
  QString sqlStatementSeed() const;

  void appendTableFieldSpecif(SqlTableFieldSpecifs *specifs);
  QString tableCreationSqlString();
};


} // namespace msXpSmineXpert
