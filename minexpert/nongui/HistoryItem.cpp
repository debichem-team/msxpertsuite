/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>

/////////////////////// Local includes
#include <minexpert/nongui/History.hpp>


namespace msXpSmineXpert
{


//! Map relating IntegrationType keys to pointers to globally-allocated
//! IntegrationTypeDesc * values
/*!
  This map is a static member of the HistoryItem class, as it is immutable
  over all the execution of the mineXpert program.
  */
QMap<int /* IntegrationType */, IntegrationTypeDesc *>
  HistoryItem::m_integrationTypeDescriptionMap;

bool HistoryItem::m_mapPrepared = false;


//! Initialize the IntegrationType vs IntegrationTypeDesc map
/*!

  The various IntegrationTypeDesc structures required to document the various
  IntegrationType elements of the IntegrationType enumeration are allocated
  and filled-in in this function. The \c
  HIstoryItem::m_integrationTypeDescriptionMap map is then filled-in. See code
  for the details.

  Note that this function is called once during the initialization of the
  msXpSmineXpert::MainWindow class.
  */
void
HistoryItem::prepareIntegrationTypeDescriptionMap()
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()";

  int intType                      = IntegrationType::NOT_SET;
  IntegrationTypeDesc *intTypeDesc = new IntegrationTypeDesc;
  intTypeDesc->brief               = "NOT_SET";
  intTypeDesc->detailed            = "Not set";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::NOT_SET:" << IntegrationType::NOT_SET
  //<< "bin:" << QString("%1").arg(IntegrationType::NOT_SET, 0, 2);

  intType               = IntegrationType::FILE_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[FILE->RT]";
  intTypeDesc->detailed = "TIC from data";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::FILE_TO_RT:" << IntegrationType::FILE_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::FILE_TO_RT, 0, 2);

  intType               = IntegrationType::FILE_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[FILE->MZ]";
  intTypeDesc->detailed = "Mass spectrum from data";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::FILE_TO_MZ:" << IntegrationType::FILE_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::FILE_TO_MZ, 0, 2);

  intType               = IntegrationType::FILE_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[FILE->DT]";
  intTypeDesc->detailed = "Drift spectrum from data";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::FILE_TO_DT:" << IntegrationType::FILE_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::FILE_TO_DT, 0, 2);

  intType               = IntegrationType::FILE_TO_XIC;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[FILE->XIC]";
  intTypeDesc->detailed = "XIC from data";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::FILE_TO_XIC:" << IntegrationType::FILE_TO_XIC
  //<< "bin:" << QString("%1").arg(IntegrationType::FILE_TO_XIC, 0, 2);

  intType               = IntegrationType::RT_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[RT->MZ]";
  intTypeDesc->detailed = "Mass spectrum from TIC chromatogram";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::RT_TO_MZ:" << IntegrationType::RT_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::RT_TO_MZ, 0, 2);

  intType               = IntegrationType::RT_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[RT->DT]";
  intTypeDesc->detailed = "Drift time spectrum from TIC chromatogram";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::RT_TO_DT:" << IntegrationType::RT_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::RT_TO_DT, 0, 2);

  intType               = IntegrationType::RT_TO_TIC_INT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[RT->TICint]";
  intTypeDesc->detailed = "TIC intensity (single value) from TIC chromatogram";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::RT_TO_TIC_INT:" << IntegrationType::RT_TO_TIC_INT
  //<< "bin:" << QString("%1").arg(IntegrationType::RT_TO_TIC_INT, 0, 2);

  intType               = IntegrationType::MZ_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ->RT]";
  intTypeDesc->detailed = "XIC chromatogram from mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_RT:" << IntegrationType::MZ_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::MZ_TO_RT, 0, 2);

  intType               = IntegrationType::MZ_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ->MZ]";
  intTypeDesc->detailed = "Mass spectrum from mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_MZ:" << IntegrationType::MZ_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::MZ_TO_MZ, 0, 2);

  intType               = IntegrationType::MZ_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ->DT]";
  intTypeDesc->detailed = "Drift time spectrum from mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_DT:" << IntegrationType::MZ_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::MZ_TO_DT, 0, 2);

  intType               = IntegrationType::MZ_TO_TIC_INT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ->TICint]";
  intTypeDesc->detailed = "TIC intensity (single value) from mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_TIC_INT:" << IntegrationType::MZ_TO_TIC_INT
  //<< "bin:" << QString("%1").arg(IntegrationType::MZ_TO_TIC_INT, 0, 2);

  intType               = IntegrationType::DT_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT->RT]";
  intTypeDesc->detailed = "XIC chromatogram from drift time spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_RT:" << IntegrationType::DT_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::DT_TO_RT, 0, 2);

  intType               = IntegrationType::DT_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT->MZ]";
  intTypeDesc->detailed = "Mass spectrum from drift time spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_MZ:" << IntegrationType::DT_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::DT_TO_MZ, 0, 2);

  intType               = IntegrationType::DT_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT->DT]";
  intTypeDesc->detailed = "Drift spectrum from drift time spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_DT:" << IntegrationType::DT_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::DT_TO_DT, 0, 2);

  intType               = IntegrationType::DT_TO_TIC_INT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT->TICint]";
  intTypeDesc->detailed = "TIC intensity (single value) from drift spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_TIC_INT:" << IntegrationType::DT_TO_TIC_INT
  //<< "bin:" << QString("%1").arg(IntegrationType::DT_TO_TIC_INT, 0, 2);

  intType               = IntegrationType::DTMZ_DT_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_DT->RT]";
  intTypeDesc->detailed = "XIC chromatogram from MZ/DT color map, DT range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_DT_TO_RT:" << IntegrationType::DTMZ_DT_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_DT_TO_RT, 0, 2);

  intType               = IntegrationType::DTMZ_MZ_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_MZ->RT]";
  intTypeDesc->detailed = "XIC chromatogram from MZ/DT color map, MZ range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_MZ_TO_RT:" << IntegrationType::DTMZ_MZ_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_MZ_TO_RT, 0, 2);

  intType               = IntegrationType::DTMZ_DT_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_DT->MZ]";
  intTypeDesc->detailed = "Mass spectrum from MZ/DT color map, DT range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_DT_TO_MZ:" << IntegrationType::DTMZ_DT_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_DT_TO_MZ, 0, 2);

  intType               = IntegrationType::DTMZ_MZ_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_MZ->MZ]";
  intTypeDesc->detailed = "Mass spectrum from MZ/DT color map, MZ range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_MZ_TO_MZ:" << IntegrationType::DTMZ_MZ_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_MZ_TO_MZ, 0, 2);

  intType               = IntegrationType::DTMZ_DT_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_DT->DT]";
  intTypeDesc->detailed = "Drift spectrum from MZ/DT color map, DT range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_DT_TO_DT:" << IntegrationType::DTMZ_DT_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_DT_TO_DT, 0, 2);

  intType               = IntegrationType::DTMZ_MZ_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DTMZ_MZ->DT]";
  intTypeDesc->detailed = "Drift spectrum from MZ/DT color map, MZ range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_MZ_TO_DT:" << IntegrationType::DTMZ_MZ_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_MZ_TO_DT, 0, 2);

  intType            = IntegrationType::DTMZ_DT_TO_TIC_INT;
  intTypeDesc        = new IntegrationTypeDesc;
  intTypeDesc->brief = "[DTMZ_DT->TICint]";
  intTypeDesc->detailed =
    "TIC intensity (single value) from MZ/DT color map, DT range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_DT_TO_TIC_INT:" <<
  //IntegrationType::DTMZ_DT_TO_TIC_INT
  //<< "bin:" << QString("%1").arg(IntegrationType::DTMZ_DT_TO_TIC_INT, 0, 2);

  intType            = IntegrationType::DTMZ_MZ_TO_TIC_INT;
  intTypeDesc        = new IntegrationTypeDesc;
  intTypeDesc->brief = "[DTMZ_MZ->TICint]";
  intTypeDesc->detailed =
    "TIC intensity (single value) from MZ/DT color map, MZ range";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DTMZ_MZ_TO_TIC_INT:" <<
  //IntegrationType::DTMZ_MZ_TO_TIC_INT;

  intType               = IntegrationType::XXX_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[XXX->RT]";
  intTypeDesc->detailed = "Xxx to TIC";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::XXX_TO_RT:" << IntegrationType::XXX_TO_RT;

  intType               = IntegrationType::XXX_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[XXX->MZ]";
  intTypeDesc->detailed = "Xxx to mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::XXX_TO_MZ:" << IntegrationType::XXX_TO_MZ;

  intType               = IntegrationType::XXX_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[XXX->DT]";
  intTypeDesc->detailed = "Xxx to drift spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::XXX_TO_DT:" << IntegrationType::XXX_TO_DT;

  intType               = IntegrationType::RT_TO_XXX;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[RT->XXX]";
  intTypeDesc->detailed = "TIC to Xxx";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::RT_TO_XXX:" << IntegrationType::RT_TO_XXX;

  intType               = IntegrationType::MZ_TO_XXX;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ->XXX]";
  intTypeDesc->detailed = "Mass spectrum to Xxx";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_XXX:" << IntegrationType::MZ_TO_XXX;

  intType               = IntegrationType::DT_TO_XXX;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT->XXX]";
  intTypeDesc->detailed = "Drift spectrum to Xxx";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_XXX:" << IntegrationType::DT_TO_XXX;

  intType               = IntegrationType::XXX_TO_TIC_INT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[XXX->TIC_INT]";
  intTypeDesc->detailed = "Xxx to TIC intensity (single value)";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::XXX_TO_TIC_INT:" << IntegrationType::XXX_TO_TIC_INT;

  intType               = IntegrationType::RT_TO_ANY;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[RT-to-any]";
  intTypeDesc->detailed = "TIC to any";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::RT_TO_ANY:" << IntegrationType::RT_TO_ANY
  //<< "bin:" << QString("%1").arg(IntegrationType::RT_TO_ANY, 0, 2);

  intType               = IntegrationType::MZ_TO_ANY;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[MZ-to-any]";
  intTypeDesc->detailed = "Mass spectrum to any";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::MZ_TO_ANY:" << IntegrationType::MZ_TO_ANY
  //<< "bin:" << QString("%1").arg(IntegrationType::MZ_TO_ANY, 0, 2);

  intType               = IntegrationType::DT_TO_ANY;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[DT-to-any]";
  intTypeDesc->detailed = "Drift spectrum to any";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::DT_TO_ANY:" << IntegrationType::DT_TO_ANY
  //<< "bin:" << QString("%1").arg(IntegrationType::DT_TO_ANY, 0, 2);

  intType               = IntegrationType::ANY_TO_RT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[any-to-RT]";
  intTypeDesc->detailed = "Any to TIC";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::ANY_TO_RT:" << IntegrationType::ANY_TO_RT
  //<< "bin:" << QString("%1").arg(IntegrationType::ANY_TO_RT, 0, 2);

  intType               = IntegrationType::ANY_TO_MZ;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[any-to-MZ]";
  intTypeDesc->detailed = "Any to Mass spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::ANY_TO_MZ:" << IntegrationType::ANY_TO_MZ
  //<< "bin:" << QString("%1").arg(IntegrationType::ANY_TO_MZ, 0, 2);

  intType               = IntegrationType::ANY_TO_DT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[any-to-DT]";
  intTypeDesc->detailed = "Any to Drift spectrum";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::ANY_TO_DT:" << IntegrationType::ANY_TO_DT
  //<< "bin:" << QString("%1").arg(IntegrationType::ANY_TO_DT, 0, 2);

  intType               = IntegrationType::ANY_TO_TIC_INT;
  intTypeDesc           = new IntegrationTypeDesc;
  intTypeDesc->brief    = "[any-to-DT]";
  intTypeDesc->detailed = "Any to TIC intensity (single value)";
  HistoryItem::m_integrationTypeDescriptionMap.insert(intType, intTypeDesc);
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__
  //<< "IntegrationType::ANY_TO_TIC_INT:" << IntegrationType::ANY_TO_TIC_INT
  //<< "bin:" << QString("%1").arg(IntegrationType::ANY_TO_TIC_INT, 0, 2);

  m_mapPrepared = true;

  // QStringList textList;
  // QList<IntegrationTypeDesc *> descList =
  // HistoryItem::m_integrationTypeDescriptionMap.values(); for(int iter = 0;
  // iter < descList.size(); ++iter)
  //{
  // textList.append(descList.at(iter)->brief);
  //}

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< textList.join("\n");
}


//! Construct a HistoryItem instance
/*!

  The instance has its date-time member datum set to current date and time and
  is further initialized by running initializeIntegrationTypes(), that
  initializes the \c integrationTypeDescriptionMap member map.

  \sa initializeIntegrationTypes()

*/
HistoryItem::HistoryItem()
{
  m_dateTime = QDateTime::currentDateTimeUtc();

  if(!HistoryItem::m_mapPrepared)
    prepareIntegrationTypeDescriptionMap();
}


//! Construct a HistoryItem instance as a copy of \p other
/*!

  \c this instance is initialized by running initializeIntegrationTypes(), that
  initializes the \c integrationTypeDescriptionMap member map.

  Further initialization is performed as a deep copy of the member data in \p
  other into \c this instance.

  \param other HistoryItem instance to use for the initialization of \c this
  instance

  \sa HistoryItem()

*/
HistoryItem::HistoryItem(const HistoryItem &other)
{
  m_dateTime = other.m_dateTime;

  if(!HistoryItem::m_mapPrepared)
    prepareIntegrationTypeDescriptionMap();

  // And now duplicate the other map.
  QList<IntegrationRange *> list = other.m_integrationMap.values();

  for(int iter = 0; iter < list.size(); ++iter)
    {
      IntegrationRange *intRange = list.at(iter);
      int intType                = other.m_integrationMap.key(intRange);

      IntegrationRange *newIntRange = new IntegrationRange;
      newIntRange->start            = intRange->start;
      newIntRange->end              = intRange->end;

      m_integrationMap.insert(intType, newIntRange);
    }
}


//! Initialize with HistoryItem \p other template.
/*!

  \return a reference to \c this HistoryItem instance.

*/
HistoryItem &
HistoryItem::initialize(const HistoryItem &other)
{
  m_dateTime = other.m_dateTime;

  // And now duplicate the map.
  QList<IntegrationRange *> list = other.m_integrationMap.values();

  for(int iter = 0; iter < list.size(); ++iter)
    {
      IntegrationRange *intRange = list.at(iter);
      int intType                = other.m_integrationMap.key(intRange);

      IntegrationRange *newIntRange = new IntegrationRange;
      newIntRange->start            = intRange->start;
      newIntRange->end              = intRange->end;

      m_integrationMap.insert(intType, newIntRange);
    }

  return *this;
}


//! Assignment operator.
/*!

  The assignment of \p other to \c this instance runs a deep copy of the
  member data in \p other into \c this instance.

  Note that the integrationTypeDescriptionMap is initialized by calling
  initializeIntegrationTypes().

  \param other HistoryItem instance to be used for the initialization of \c
  this instance.

  \return a reference to \c this HistoryItem instance.

*/
HistoryItem &
HistoryItem::operator=(const HistoryItem &other)
{
  return this->initialize(other);
}


//! Destruct \c this HistoryItem instance
/*!

  All the heap-allocated IntegrationRange and IntegrationTypeDesc instances
  are deleted.

*/
HistoryItem::~HistoryItem()
{
  QList<IntegrationRange *> irList = m_integrationMap.values();

  while(irList.size())
    delete irList.takeFirst();

  // Finally, clear the map.
  m_integrationMap.clear();
}


//! Equality operator.
/*!

  Two HistoryItem instances are equal if all their members are identical. Note
  that the order of the items in the \c m_integrationMap is not relevant and
  that the \c integrationTypeDescriptionMap is not checked since it is
  considered immutable.

  Note that the IntegrationType instances pointed to by items in the \c
  m_integrationMap map are tested for \c start and \c end values, not by
  pointer value.

  \param other reference to a HistoryItem instance to compare to \c this
  instance.

  \return true if both HistoryItem instances are identical.

  \sa contains()

*/
bool
HistoryItem::operator==(const HistoryItem &other) const
{
  if(this == &other)
    return true;

  if(m_dateTime != other.m_dateTime)
    return false;

  // Now we need to compare all the pairs in the map.

  if(m_integrationMap.size() != other.m_integrationMap.size())
    return false;

  QList<int> intTypeList = m_integrationMap.keys();

  for(int iter = 0; iter < intTypeList.size(); ++iter)
    {
      int intType = intTypeList.at(iter);

      IntegrationRange *intRange = m_integrationMap.value(intType);

      if(other.contains(intType, *intRange))
        continue;
      else
        return false;
    }

  return true;
}


//! Verifies if the \c m_integrationMap contains an item
/*!

  The \c m_integrationMap map is checked for having a value mapped to the \p
  intType parameter that has the same IntegrationRange values start and end as
  the \p &intRange.

  \param intType IntegrationType key to search in the \c m_integrationMap.

  \param intRange reference to an IntegrationRange instance to be used for the
  comparison.

  \return True if an IntegrationRange instance is found as value in \c
  m_integrationMap that has the same \c start and \c end member values as \p
  &intRange.

  \sa operator==()

*/
bool
HistoryItem::contains(int intType, const IntegrationRange &intRange) const
{
  IntegrationRange *localRange = m_integrationMap.value(intType, Q_NULLPTR);

  if(localRange == Q_NULLPTR)
    return false;

  if(localRange->start == intRange.start && localRange->end == intRange.end)
    return true;

  return false;
}


//! Set the date and time to the current date and time
/*!

  The date and time format is UTC (universal time coordinated).

*/
void
HistoryItem::setDateTime()
{
  m_dateTime = QDateTime::currentDateTimeUtc();
}


//! Get the date and time of \c this HistoryItem instance.
/*!

  \return The date and time in QDateTime format.

  \sa setDateTime()

*/
QDateTime
HistoryItem::dateTime() const
{
  return m_dateTime;
}


//! Checks if \c this HistoryItem instance has an IntegrationType
/*!

  The \c m_integrationMap map is queried for a key having the same value as \p
  integrationType.

  \param integrationType IntegrationType value to search as a key of the \c
  m_integrationMap map.

  \return true if \c m_integrationMap has at an integration type identical to
  \p integrationType; false otherwise.

  \sa

*/
bool
HistoryItem::hasIntegrationType(int integrationType)
{
  QList<int> list = m_integrationMap.keys();

  if(list.contains(integrationType))
    return true;

  return false;
}


//! Creates a new IntegrationRange instance and inserts it in the member map
/*!

  \param intType IntegrationType value used for the map insertion

  \param start start of range used for the creation of the IntegrationRange
  instance

  \param end end of range used for the creation of the IntegrationRange instance


*/
void
HistoryItem::newIntegrationRange(int intType, double start, double end)
{
  IntegrationRange *intRange = new IntegrationRange;
  intRange->start            = start;
  intRange->end              = end;

  m_integrationMap.insert(intType, intRange);
}


IntegrationRange *
HistoryItem::integrationRange(int intType) const
{
  IntegrationRange *intRange = m_integrationMap.value(intType, Q_NULLPTR);
  return intRange;
}


int
HistoryItem::integrationType(IntegrationRange *integrationRange)
{
  if(integrationRange == Q_NULLPTR)
    qFatal(
      "Fatal error at %s@%d -- %s. "
      "Q_NULLPTR."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  return m_integrationMap.key(integrationRange);
}


//! Get a list of IntegrationType values matching the integration ranges.
/*!

  A given HistoryItem instance might have more than one integration range, and
  for each one of these integration ranges, it might have different
  IntegrationType values. This function returns the list of keys of the \c
  m_integrationMap QMap<IntegrationType, IntegrationRange *> map.

  One typical situation is when performing an integration from the color map
  widget, that is, integrating using two ranges: the mz range and the dt range
  (ion mobility mass spectrometry only).

*/
QList<int>
HistoryItem::integrationTypes() const
{
  return m_integrationMap.keys();
}


QList<IntegrationRange *>
HistoryItem::integrationRanges() const
{
  return m_integrationMap.values();
}


bool
HistoryItem::isValid() const
{
  if(!m_dateTime.isValid())
    return false;

  if(m_integrationMap.isEmpty())
    return false;

  return true;
}


QString
HistoryItem::asText(bool brief) const
{
  QString text;

  // For each integration range in the map, craft a text element.

  QList<IntegrationRange *> list = m_integrationMap.values();

  // qDebug() << __FILE__ << __LINE__ << "There are " << list.size()
  //<< " integration ranges in the map";

  for(int iter = 0; iter < list.size(); ++iter)
    {
      IntegrationRange *intRange = list.at(iter);
      int intType                = m_integrationMap.key(intRange);

      // qDebug() << __FILE__ << __LINE__
      //<< "Currently iterated intType:" << intType;

      IntegrationTypeDesc *intTypeDesc =
        m_integrationTypeDescriptionMap.value(intType);

      if(intTypeDesc == Q_NULLPTR)
        qFatal(
          "Fatal error at %s@%d. Not possible that an integration type has no "
          "corresponding description text. "
          "Program aborted.",
          __FILE__,
          __LINE__);

      QString label;

      if(brief)
        label = intTypeDesc->brief + " ";
      else
        label = intTypeDesc->detailed + ":\n";

      if(brief)
        {
          text += label + QString("range: [%1-%2]")
                            .arg(intRange->start, 0, 'f', 3)
                            .arg(intRange->end, 0, 'f', 3);

          if(iter > 0 && iter < list.size() - 1)
            text += " | ";
          if(iter == list.size() - 1)
            text += "\n";
        }
      else
        text += label + QString("\tRange: [%1-%2]\n")
                          .arg(intRange->start, 0, 'f', 3)
                          .arg(intRange->end, 0, 'f', 3);
    }

  // Finally, only print the date time if we are verbose.
  if(!brief)
    {
      text += "\t" + m_dateTime.toString(Qt::ISODate);
      text += "\n";
    }


  // qDebug() << __FILE__ << __LINE__ << "text: " << text;

  return text;
}


} // namespace msXpSmineXpert
