/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QList>
#include <QDateTime>
#include <QMap>


/////////////////////// Local includes
#include <globals/globals.hpp>
#include <minexpert/nongui/HistoryItem.hpp>
#include <minexpert/nongui/MzIntegrationParams.hpp>


namespace msXpSmineXpert
{


//! The History class provides a means to store an integration history
/*!

  When the user performs data integration in the course of mass spectrometric
  data analysis, it is necessary that the various integration steps be stored
  as meaningful data bits, both for the program to know where a given trace
  (mass spec, drift spec or chromatogram, for exemple) originates from and for
  the user to understand the various data mining results that are a result of
  the way the data integrations were performed in sequence during the data
  mining session.

  The History class provides a means to store a list of HistoryItem instances
  (heap-allocated instances). The use of the History class is to typically
  attach one instance to each plot widget used in the mineXpert program so
  that any given widget knows the history of the data it plots.

  \sa HistoryItem

*/
class History
{

  private:
  //! List of HistoryItem instances
  /*!

    The instances listed in the list are heap-allocated and the stored data
    are in fact pointers to HistoryItem instances.

*/
  QList<HistoryItem *> m_historyItemList;

  MzIntegrationParams m_mzIntegrationParams;

  public:
  History();
  History(const History &other);
  History &operator=(const History &other);
  ~History();

  bool isEmpty();

  void setMzIntegrationParams(MzIntegrationParams params);
  MzIntegrationParams mzIntegrationParams() const;

  void freeList();

  void copyHistory(const History &history, bool noDuplicates = true);
  int appendHistoryItem(HistoryItem *item);
  int copyHistoryItem(const HistoryItem &item);

  HistoryItem *
  firstHistoryItem(int integrationType,
                   IntegrationRange **integrationRange = Q_NULLPTR);

  HistoryItem *lastHistoryItem(int integrationType,
                               IntegrationRange **integrationRange = Q_NULLPTR);

  const HistoryItem *newestHistoryItem() const;

  int containsHistoryItem(const HistoryItem &item);

  bool innermostRtRange(double *start, double *end) const;
  bool innermostMzRange(double *start, double *end) const;
  bool innermostDtRange(double *start, double *end) const;
  bool innermostRange(int integrationType, double *start, double *end) const;

  bool innermostRange(double *start, double *end) const;

  QString innermostRangesAsText() const;

  bool isValid() const;

  QString asText(const QString &header = QString(),
                 const QString &footer = QString(),
                 bool brief            = false) const;
};


} // namespace msXpSmineXpert
