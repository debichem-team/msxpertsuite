/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <QString>

namespace msXpSmineXpert
{

//! This enum documents the type of the data in Qt parlance.
/*!

  The types of the database table fields are documented in this enum. For the
  naming, we use the Qt parlance.

*/
enum FieldQtType
{
  QT_TYPE_NOT_SET = 0,
  /*!< The field type is not set.*/
  QSTRING,
  /*!< The field type is QString.*/
  INT,
  /*!< The field type is integer.*/
  DOUBLE,
  /*!< The field type is double.*/
  BINARY,
  /*!< The field type is binary.*/
};


//! This enum documents the type of the data in SQL parlance.
/*!

  The types of the database table fields are documented in this enum. For the
  naming, we use the SQL parlance.

*/
enum FieldSqlType
{
  SQL_TYPE_NOT_SET = 0,
  /*!< The field type is not set.*/
  TEXT,
  /*!< The field type is text string.*/
  INTEGER,
  /*!< The field type is integer.*/
  FLOAT,
  /*!< The field type is float/double.*/
  BLOB,
  /*!< The field type is blob/binary.*/
};


//! The SqlTableFieldSpecifs class provides a configuration for a SQL database
//! table field.
/*!

  When creating a new mass spectrometry data SQLite3-based formatted file, the
  tables need to be created as a very first step. This class provides a
  configuration mechanism that is used to configure a field of a
  database table.

*/
class SqlTableFieldSpecifs
{
  private:
  //! Name of the table.
  QString m_tableName;

  //! Name of the field.
  QString m_fieldName;

  //! Type of the data stored for this field (Qt-based naming scheme).
  FieldQtType m_fieldQtType;

  //! Type of the data stored for this field (SQL-based naming scheme).
  FieldSqlType m_fieldSqlType;

  //! Comment associated to the field.
  QString m_comment;

  //! Command line.
  QString m_sqlStatement;

  public:
  SqlTableFieldSpecifs(const QString &tableName    = QString(),
                       const QString &fieldName    = QString(),
                       FieldQtType                 = QT_TYPE_NOT_SET,
                       FieldSqlType                = SQL_TYPE_NOT_SET,
                       const QString &comment      = QString(),
                       const QString &sqlStatement = QString());

  SqlTableFieldSpecifs(const QString &tableName);

  ~SqlTableFieldSpecifs();

  void setTableName(QString tableName);
  QString tableName() const;

  void setFieldName(QString fieldName);
  QString fieldName() const;

  void setFieldQtType(FieldQtType type);
  FieldQtType fieldQtType() const;

  void setFieldSqlType(FieldSqlType type);
  FieldSqlType fieldSqlType() const;

  void setComment(QString comment);
  QString comment() const;

  void setSqlStatement(QString sqlStatement);
  QString sqlStatement() const;
};

} // namespace msXpSmineXpert
