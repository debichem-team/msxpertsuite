/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>


/////////////////////// Local includes


namespace msXpSmineXpert
{


//! The FileFormatConverter class provides mzML to SQLite3 conversion
/*!

  mineXpert can read mass spectrometry data files in a number of file format,
  in part thanks to the libpwiz library from the Proteowizard project. An open
  private file format based on SQLite3 was also developed and proves to be
  useful for speedier loading of files. Also, when the SQLite3 format is used,
  it is possible to load a file and then slice that file into smaller chunks
  retaining all the initial information (apart from being smaller, that is).

*/
class FileFormatConverter : public QObject
{
  Q_OBJECT;

  private:
  //! Name of the file in input (mzML format)
  QString m_inFileName;

  //! Name of the file in output (SQLite3 db format)
  QString m_outFileName;

  public:
  FileFormatConverter(QObject *parent = Q_NULLPTR);
  virtual ~FileFormatConverter();

  void setInFileName(const QString &fileName);
  QString inFileName() const;

  void setOutFileName(const QString &fileName);
  QString outFileName() const;

  void setParent(QObject *parent);
  void craftDbFileName();
  bool convert(QString *errors);
};


} // namespace msXpSmineXpert
