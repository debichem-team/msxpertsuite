/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

////////////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QTextStream>


/*
   Bitwise stuff

   It is sometimes worth using an enum to name the bits:

   enum ThingFlags = {
   ThingMask = 0x0000,
   ThingFlag0 = 1 << 0,
   ThingFlag1 = 1 << 1,
   ThingError = 1 << 8,
   }

   Then use the names later on. I.e. write

   thingstate |= ThingFlag1;
   thingstate &= ~ThingFlag0;
   if (thing & ThingError) {...}

   to set, clear and test. This way you hide the magic numbers from the rest of
   your code.
   */


namespace msXpSmineXpert
{

//! Enumeration defining where the message logging must be directed at.
/*!

  mineXpert might log a number of feedback messages. Depending on the values
  selected, the output is directed to different places.

*/
enum LogType
{
  LOG_TO_NONE    = 0x0,
  LOG_TO_CONSOLE = 1 << 0,
  /*!< Append output to the console (the console window, not the terminal).*/

  LOG_TO_CONSOLE_OVERWRITE = 1 << 1,
  /*!< Overwrite output to the console (the console window, not the terminal).*/

  LOG_TO_STATUS_BAR = 1 << 2,
  /*!< Show output to the status bar of the window.*/

  LOG_TO_BOTH           = (LOG_TO_CONSOLE | LOG_TO_STATUS_BAR),
  LOG_TO_BOTH_OVERWRITE = (LOG_TO_CONSOLE_OVERWRITE | LOG_TO_STATUS_BAR),
};


//! Enumeration defining the kind of mass spectra.
/*!

  When loading or handling mass spectrometry data, mineXpert needs to know if
  the data are for mass spectrometry experiments or for ion mobility mass
  spectrometry experiments.

*/
enum MassDataKind
{
  MASS_SPECTRA = 0,
  /*!< Mass spectrometry data.*/
  DRIFT_SPECTRA,
  /*!< Ion mobility mass spectrometry data.*/
};


//! Enumeration defining the plot axis/axes.
enum PlotAxis
{
  NONE_AXIS = 1 << 0,
  /*!< The plot axis is undefined.*/
  X_AXIS = 1 << 1,
  /*!< The abscissa axis.*/
  Y_AXIS = 1 << 2,
  /*!< The ordinates axis.*/
  BOTH_AXES = (X_AXIS | Y_AXIS)
};


//! Enumeration defining the mass spectrometry file data format.
/*!

  This enumeration is used to define the mass spectrometry file data format so
  the proper loader is selected. The enum names are self-explanatory.

*/
enum MassSpecDataFileFormat
{

  // The strategy is to put the most complex formats but easily identified on
  // top, so that when iterating in all the value of the enum, it is a quick
  // operation to get to the proper format (see class
  // MassSpecDataFileFormatAnalyzer).

  MASS_SPEC_DATA_FILE_FORMAT_NOT_SET = 0,
  MASS_SPEC_DATA_FILE_FORMAT_SQLITE3,
  MASS_SPEC_DATA_FILE_FORMAT_MZML,
  MASS_SPEC_DATA_FILE_FORMAT_MZXML,
  MASS_SPEC_DATA_FILE_FORMAT_DX,
  MASS_SPEC_DATA_FILE_FORMAT_MS1,
  MASS_SPEC_DATA_FILE_FORMAT_BRUKER_XY,
  MASS_SPEC_DATA_FILE_FORMAT_XY,
  MASS_SPEC_DATA_FILE_FORMAT_LAST,
};


} // namespace msXpSmineXpert
