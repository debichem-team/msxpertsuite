/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QString>
#include <QHash>


/////////////////////// Local includes
#include <minexpert/nongui/globals.hpp>

namespace msXpSmineXpert
{


//! This enum documents the kind of data element that is stored.
/*!

  The enum members are self-explanatory.

*/
enum MassSpecFileMetaDataType
{
  MASS_SPEC_META_DATA_TYPE_NOT_SET = 0 << 0,
  MASS_SPEC_META_DATA_TYPE_STRING  = 1 << 0,
  MASS_SPEC_META_DATA_TYPE_INT     = 1 << 1,
  MASS_SPEC_META_DATA_TYPE_DOUBLE  = 1 << 2,
  MASS_SPEC_META_DATA_TYPE_BOOL    = 1 << 3,
  MASS_SPEC_META_DATA_TYPE_ALL =
    (MASS_SPEC_META_DATA_TYPE_STRING | MASS_SPEC_META_DATA_TYPE_INT |
     MASS_SPEC_META_DATA_TYPE_DOUBLE | MASS_SPEC_META_DATA_TYPE_BOOL),
};


class MassSpecFileMetaData
{
  private:
  //! Hash relating string keys with string values.
  QHash<QString, QString> m_stringHash;

  //! Hash relating string keys with integer values.
  QHash<QString, int> m_intHash;

  //! Hash relating string keys with double values.
  QHash<QString, double> m_doubleHash;

  //! Hash relating string keys with boolean values.
  QHash<QString, bool> m_boolHash;

  public:
  MassSpecFileMetaData();
  ~MassSpecFileMetaData();

  MassSpecFileMetaData &operator=(const MassSpecFileMetaData &other);

  bool stringItem(QString key, QString &value) const;
  int allStringItems(QStringList &keys, QStringList &values) const;
  void setStringItem(QString key, QString value);

  bool intItem(QString key, int &value) const;
  int allIntItems(QStringList &keys, QList<int> &values) const;
  void setIntItem(QString key, int value);

  bool doubleItem(QString key, double &value) const;
  int allDoubleItems(QStringList &keys, QList<double> &values) const;
  void setDoubleItem(QString key, double value);

  bool boolItem(QString key, bool &value) const;
  int allBoolItems(QStringList &keys, QList<bool> &values) const;
  void setBoolItem(QString key, bool value);

  QString *asText(MassSpecFileMetaDataType type) const;
};

} // namespace msXpSmineXpert
