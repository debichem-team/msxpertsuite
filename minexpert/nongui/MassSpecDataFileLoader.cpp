/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QObject>
#include <QMainWindow>
#include <QHash>
#include <QMap>
#include <QDebug>


/////////////////////// Local includes
#include <minexpert/nongui/MassSpecDataFileLoader.hpp>
#include <minexpert/nongui/globals.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>


namespace msXpSmineXpert
{


//! Construct a MassSpecDataFileLoader instance.
/*!

  \param Name of the file to load.

*/
MassSpecDataFileLoader::MassSpecDataFileLoader(const QString &fileName)
{
  if(!fileName.isEmpty())
    m_fileName = fileName;
}


//! Destruct \c this MassSpecDataFileLoader instance.
MassSpecDataFileLoader::~MassSpecDataFileLoader()
{
}


//! Copy \p massSpecDataStats to this instance
void
MassSpecDataFileLoader::setMassSpecDataStats(
  const MassSpecDataStats &massSpecDataStats)
{
  m_massSpecDataStats = massSpecDataStats;
}


//! Set the name of the file to load.
/*!

  \param fileName name of the file.

*/
void
MassSpecDataFileLoader::setFileName(const QString &fileName)
{
  m_fileName = fileName;
}


//! Append an error message to the string list.
/*!

  Append a new error message string to \c m_errorMessageList.

  \param errorMsg new message string.

*/
void
MassSpecDataFileLoader::appendErrorMessage(QString errorMsg)
{
  m_errorMessageList.append(errorMsg);
}


//! Cancel the running operation.
/*!

  This function can be invoked to ask the loader to stop loading mass data from
  the file.

*/
void
MassSpecDataFileLoader::cancelOperation()
{
  m_isOperationCancelled = true;

  // qDebug() << __FILE__ << __LINE__
  //<< "MassSpecDataFileLoader::cancelOperation";
}


//! Tell if the operation was cancelled.
/*!

  This function can be used to enquire if the user has cancelled the data
  loading operation. It is typically used from inside a \e for loop. If true,
  the loop would exit.

  \return true if the \c m_isOperationCancelle value is true.

  \sa cancelOperation().

*/
bool
MassSpecDataFileLoader::isOperationCancelled()
{
  return m_isOperationCancelled;
}


} // namespace msXpSmineXpert
