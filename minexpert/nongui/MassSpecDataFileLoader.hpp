/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QMainWindow>
#include <QStringList>
#include <QHash>
#include <QMap>


/////////////////////// Local includes
#include <minexpert/nongui/globals.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>
#include <minexpert/nongui/History.hpp>
#include <minexpert/nongui/MassSpecDataStats.hpp>

namespace msXpSmineXpert
{

//! The MassSpecDataFileLoader provides a mass spectrometry data file loader.
/*!

  The MassSpecDataFileLoader implements a base class for specialized mass
  spectrometry data file loaders.

*/
class MassSpecDataFileLoader : public QObject
{

  Q_OBJECT

  protected:
  //! Name of the mass data file from which to load mass spectrometry data.
  QString m_fileName = "NOT_SET";

  //! List of strings containing the various error messages encountered during
  //! operation.
  QStringList m_errorMessageList;

  //! String containing the spectrum title.
  QString m_spectrumTitle = "NOT_SET";

  //! Start value for the feedback progress.
  int m_progressFeedbackStartValue = -1;

  //! End value for the feedback progress.
  int m_progressFeedbackEndValue = -1;

  //! Number of spectra detected in the data file.
  int m_spectrumCount = -1;

  // This switch is useful for the owner of an instance of this class to
  // stop the current operation.
  bool m_isOperationCancelled = false;

  //! Tells if the binning setup was performed already for streamed mode mz
  //! integrations
  bool m_isBinningSetup = false;

  //! Mass spectral data statistics for use while loading mass data in stream
  //! mode
  MassSpecDataStats m_massSpecDataStats;

  public:
  //! Tells if the feedback should go the the standard output.
  bool m_feedbackToStdout = false;

  MassSpecDataFileLoader(const QString &fileName = QString());
  virtual ~MassSpecDataFileLoader();

  void setMassSpecDataStats(const MassSpecDataStats &massSpecDataStats);

  void appendErrorMessage(QString errorMsg);
  void setFileName(const QString &fileName);
  virtual QString formatDescription() const = 0;
  virtual int readSpectrumCount()           = 0;

  void cancelOperation();
  bool isOperationCancelled();

  signals:
  void updateFeedbackSignal(QString msg,
                            int currentValue = -1,
                            bool setVisible  = true,
                            int logType      = LogType::LOG_TO_CONSOLE,
                            int startValue   = -1,
                            int endValue     = -1);
};
} // namespace msXpSmineXpert
