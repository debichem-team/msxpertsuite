/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QVector>


/////////////////////// Local includes


namespace msXpSmineXpert
{


//! Parameters for the Savitzky-Golay filter
struct SavGolParams
{
  int nL = 15;
  //!< number of data points on the left of the filtered point
  int nR = 15;
  //!< number of data points on the right of the filtered point
  int m = 4;
  //!< order of the polynomial to use in the regression analysis leading to the
  //!< Savitzky-Golay coefficients (typically between 2 and 6)
  int lD = 0;
  //!< specifies the order of the derivative to extract from the Savitzky-Golay
  //!< smoothing algorithm (for regular smoothing, use 0)
  bool convolveWithNr = false;
  //!< set to false for best results
};


//! The SavGolFilter class implements the Savitzky-Golay filter
class SavGolFilter
{

  public:
  //! Parameters to configure the Savitzky-Golay filter algorithm
  SavGolParams m_savGolParams;

  //! Number of data points in the trace to filter
  int m_samples;

  //! C array of keys of the Trace
  double *m_x;

  //! C array of raw values of the Trace
  double *m_yr;

  //! C array of filtered values after the computation has been performed
  double *m_yf;

  SavGolFilter();
  SavGolFilter(const SavGolFilter &other);
  SavGolFilter(int nL, int lR, int m, int lD, bool convolveWithNr = false);
  SavGolFilter(SavGolParams savGolParams);

  virtual ~SavGolFilter();

  void filteredData(QVector<double> &data);

  virtual SavGolFilter &operator=(const SavGolFilter &other);
  void initialize(const SavGolFilter &other);
  void initialize(const SavGolParams &other);

  virtual int initializeData(const QVector<double> &keyData,
                             const QVector<double> &valData);

  int *ivector(long nl, long nh);
  double *dvector(long nl, long nh);
  double **dmatrix(long nrl, long nrh, long ncl, long nch);
  void free_ivector(int *v, long nl, long nh);
  void free_dvector(double *v, long nl, long nh);
  void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch);
  void lubksb(double **a, int n, int *indx, double b[]);
  void ludcmp(double **a, int n, int *indx, double *d);
  void four1(double data[], unsigned long nn, int isign);
  void twofft(double data1[],
              double data2[],
              double fft1[],
              double fft2[],
              unsigned long n);
  void realft(double data[], unsigned long n, int isign);
  char convlv(double data[],
              unsigned long n,
              double respns[],
              unsigned long m,
              int isign,
              double ans[]);
  char sgcoeff(double c[], int np, int nl, int nr, int ld, int m);
  char filter();

  QString asText() const;
};


} // namespace msXpSmineXpert


Q_DECLARE_METATYPE(msXpSmineXpert::SavGolParams);
Q_DECLARE_METATYPE(msXpSmineXpert::SavGolFilter);
Q_DECLARE_METATYPE(msXpSmineXpert::SavGolFilter *);

extern int savGolFilterMetaTypeId;
extern int savGolParamsMetaTypeId;
