/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MASS_SPEC_DATA_FILE_LOADER_BRUKER_XY
#define MASS_SPEC_DATA_FILE_LOADER_BRUKER_XY

////////////////////////////// Qt includes
#include <QStringList>


////////////////////////////// local includes
#include <minexpert/nongui/MassSpecDataFileLoader.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>
#include <minexpert/nongui/History.hpp>


namespace msXpSmineXpert
{

class MassSpecDataFileLoaderBrukerXy : public MassSpecDataFileLoader
{
  public:
  MassSpecDataFileLoaderBrukerXy(const QString &fileName = QString());

  ~MassSpecDataFileLoaderBrukerXy();

  static QString craftSpectrumHeaderRegExpString();
  static QString craftSpectrumLineRegExpString();

  static bool canLoadData(QString fileName);

  QString formatDescription() const override;

  int readSpectrumCount() override;

  msXpSlibmass::MassSpectrum *parseSpectrum(QString line);

  int loadData(MassSpecDataSet *massSpecDataSet);

  virtual void cancelOperation();

  int streamedIntegration(const History &history,
                          IntegrationType integrationType,
                          QVector<double> *keyVector,
                          QVector<double> *valVector);

  int streamedMzIntegration(const History &history,
                            QVector<double> *keyVector,
                            QVector<double> *valVector);
};

} // namespace msXpSmineXpert

#endif /* MASS_SPEC_DATA_FILE_LOADER_BRUKER_XY */
