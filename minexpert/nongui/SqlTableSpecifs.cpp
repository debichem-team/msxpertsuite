/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


// Qt includes
#include <QDebug>


// Local includes
#include <minexpert/nongui/SqlTableSpecifs.hpp>


namespace msXpSmineXpert
{


//! Construct a SqlTableSpecifs instance.
/*!

  \param tableName name of the table to be configured.

  \param comment comment to associated to the table.

  \param sqlStatementSeed SQL statement seed to be used to actually create
  the table after completion with all field specifications.

 */
SqlTableSpecifs::SqlTableSpecifs(QString tableName,
                                 QString comment,
                                 QString sqlStatementSeed)
{
  m_tableName        = tableName;
  m_comment          = comment;
  m_sqlStatementSeed = sqlStatementSeed;
}


//! Construct a SqlTableSpecifs instance.
/*!

  \param tableName  name of the table to be configured.

 */
SqlTableSpecifs::SqlTableSpecifs(const QString &tableName)
{
  m_tableName = tableName;
}


//! Destruct the SqlTableSpecifs instance.
/*!

  All the SqlTableFieldSpecifs instances stored in \c
  m_sqlTableFieldSpecifList are deleted.

  */
SqlTableSpecifs::~SqlTableSpecifs()
{
  clearTableFieldSpecif();
}


//! Delete all the SqlTableFieldSpecifs instances in the \c
//! m_sqlTableFieldSpecifList list.
void
SqlTableSpecifs::clearTableFieldSpecif()
{
  while(m_sqlTableFieldSpecifList.size())
    delete(m_sqlTableFieldSpecifList.takeFirst());
}


//! Delete all the SqlTableFieldSpecifs instances and reset to empty string the
//! other members.
void
SqlTableSpecifs::reset()
{
  clearTableFieldSpecif();
  m_tableName        = "";
  m_comment          = "";
  m_sqlStatementSeed = "";
}

//! Set the name of the table.
void
SqlTableSpecifs::setTableName(QString tableName)
{
  m_tableName = tableName;
}


//! Get the name of the table.
QString
SqlTableSpecifs::tableName() const
{
  return m_tableName;
}


//! Set the comment of the table.
void
SqlTableSpecifs::setComment(QString comment)
{
  m_comment = comment;
}


//! Get the comment of the table.
QString
SqlTableSpecifs::comment() const
{
  return m_comment;
}


//! Set the SQL statement seed used to create the table.
void
SqlTableSpecifs::setSqlStatementSeed(QString sqlStatementSeed)
{
  m_sqlStatementSeed = sqlStatementSeed;
}


//! Get the SQL statement seed used to create the table.
QString
SqlTableSpecifs::sqlStatementSeed() const
{
  return m_sqlStatementSeed;
}


//! Append new SqlTableFieldSpecifs instance to the list.
/*!

  The \p specifs SqlTableFieldSpecifs instance is appended to the \c
  m_sqlTableFieldSpecifList list. \p specifs cannot be nullptr.

 */
void
SqlTableSpecifs::appendTableFieldSpecif(SqlTableFieldSpecifs *specifs)
{
  if(specifs == nullptr)
    qFatal(
      "Fatal error at %s@%d -- %s. "
      "Pointer cannot be nullptr."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  m_sqlTableFieldSpecifList.append(specifs);
}


//! Craft a string containing the full table creation SQL statement.
/*!

  The \c m_sqlStatementSeed string contains the SQL statement seed. That is
  copied to a local string and then all the SqlTableFieldSpecifs items in \c
  m_sqlTableFieldSpecifList are asked to produce a string with the
  corresponding SQL statement. These strings are appended to the local SQL
  statement local string that is returned.

  \return a string with the full table creation SQL statement.

 */
QString
SqlTableSpecifs::tableCreationSqlString()
{
  // Note that all the command lines in the fieldMetaData items are not
  // punctuated, which means that we have to
  // make sure we make a join with ','.

  // m_sqlStatementSeed is the string that holds the section of the sql command
  // that actually creates the table and the id number of their items, like
  // "CREATE TABLE table(table_id INTEGER PRIMARY KEY AUTOINCREMENT NOT
  // NULL" and that is followed by the specification of all the fields of
  // the table.

  QString sqlString = m_sqlStatementSeed;
  for(int iter = 0; iter < m_sqlTableFieldSpecifList.size(); ++iter)
    {
      SqlTableFieldSpecifs *curItem = m_sqlTableFieldSpecifList.at(iter);

      QString sqlCommand = curItem->sqlStatement();

      sqlString += QString(", %1").arg(sqlCommand);
    }

  // We need to close the sql command line:
  sqlString += ")";

  return sqlString;
}


} // namespace msXpSmineXpert
