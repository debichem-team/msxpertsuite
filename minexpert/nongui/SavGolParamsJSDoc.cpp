/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

// This is a JS-doc-only file

/*/js/ Class: SavGolParams
 * <comment>This class handles the parameters of a Savitzy-Golay filter.
 *
 * A SavGolParams object contains the following member data:
 * - nl: <Number> of points on the left of the filtered point;
 * - nR: <Number of points on the right of the filtered point;
 * - m: <Number> (order) of the polynomial to use in the regression analysis
 *   leading to the Savitzky-Golay coefficients (typically between 2 and 6);
 * - lD: <Number> specifying the order of the derivative to extract from the
 *   Savitzky-Golay smoothing algorithm (for regular smoothing, use 0)
 * - convolveWithNr <Boolean>  Set to false for best results
 * </comment>
 */


/*/js/
 * SavGolParams()
 *
 * Constructor of a SavGolParams object
 *
 * Ex:
 *
 * var sgp1 = new SavGolParams();
 * sgp1.nL; // --> default 15
 * sgp1.nR; // --> default 15
 * sgp1.m; // --> default 4
 * sgp1.lD; // --> default 0
 * sgp1.convolveWithNr; // --> default false
 */


/*/js/
 * SavGolParams(savGolParams)
 *
 * Constructor of a SavGolParams object using a <SavGolParams> object as
 * template
 *
 * Ex:
 *
 * var sgp1 = new SavGolParams();
 * sgp1.nL = 10;
 * sgp1.nR = 10;
 * sgp1.m; // --> 4
 * sgp1.lD; // --> 0
 * sgp1.convolveWithNr; // --> false
 *
 * var sgp2 = new SavGolParams(sgp1);
 *
 * sgp2.nL; // --> 10
 * sgp2.nR; // --> 10
 * sgp2.m; // --> 4
 * sgp2.lD; // --> 0
 * sgp2.convolveWithNr; // --> false
 */


/*/js/
 * SavGolParams(savGolFilter)
 *
 * Constructor of a SavGolParams object using a <SavGolFilter> object as
 * template
 *
 * Ex:
 *
 * var sgf1 = new SavGolFilter();
 * sgf1.nL = 10;
 * sgf1.nR = 10;
 * sgf1.m; // --> 4
 * sgf1.lD; // --> 0
 * sgf1.convolveWithNr; // --> false
 *
 * var sgp2 = new SavGolParams(sgf1);
 *
 * sgp2.nL; // --> 10
 * sgp2.nR; // --> 10
 * sgp2.m; // --> 4
 * sgp2.lD; // --> 0
 * sgp2.convolveWithNr; // --> false
 */


/*/js/
 * SavGolParams(nL, nR, m, lD, convolveWithNr)
 *
 * Constructor of a SavGolParams object using the various configurations
 * <Number> and <Boolean> parameters.
 *
 * Ex:
 *
 * var sgf1 = new SavGolParams(10, 10, 6, 0, true);
 * sgf1.nL; // --> 10
 * sgf1.nR; // --> 10;
 * sgf1.m; // --> 6
 * sgf1.lD; // --> 0
 * sgf1.convolveWithNr; // --> true
 */
