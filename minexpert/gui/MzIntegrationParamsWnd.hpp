/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QMainWindow>
#include <QObject>


/////////////////////// Local includes
#include "ui_MzIntegrationParamsWnd.h"
#include <minexpert/gui/AbstractPlotWidget.hpp>
#include <minexpert/gui/AbstractMultiPlotWnd.hpp>
#include <globals/globals.hpp>
#include <minexpert/nongui/MzIntegrationParams.hpp>
#include <minexpert/nongui/SavGolFilter.hpp>


namespace msXpSmineXpert
{


//! The MzIntegrationParamsWnd class provides an interface to the MZ integration
//! settings.
/*!

  The MZ integration settings allow the user to configure how the integration
  of multiple mass spectra into a single mass spectrum is to be performed.
  Depending on the instrument that generated the data in the first place, the
  data in the mass spectra arre more or less amenable to integration
  (combination in mass spec parlance). The parameters in this window allow the
  user to configure most finely the way the mass spectra integrations are to
  be handled; so as to generate the best resulting mass spectrum.

*/
class MzIntegrationParamsWnd : public QMainWindow
{
  Q_OBJECT

  protected:
  Ui::MzIntegrationParamsWnd m_ui;

  private:
  MzIntegrationParams m_mzIntegrationParams;

  void initialize();

  public:
  MzIntegrationParamsWnd(QWidget *parent);
  ~MzIntegrationParamsWnd();

  void readSettings();
  void writeSettings();

  void updateMzIntegrationParams(MzIntegrationParams params,
                                 const QColor &color = QColor());

  Q_INVOKABLE void show();
  Q_INVOKABLE void setBinningType(msXpS::BinningType binningType);
  Q_INVOKABLE void setBinSize(double value);
  Q_INVOKABLE void setBinSizeType(msXpS::MassToleranceType type);
  Q_INVOKABLE void setApplyMzShift(bool apply);
  Q_INVOKABLE void setRemoveZeroValDataPoints(bool apply);

  public slots:
  void closeEvent(QCloseEvent *event);

  void binningRadioButtonToggled(bool checked);

  void binSizeTypeItemChanged(int index);
  void binSizeValueChanged(double value);
  void applyMzShiftCheckBoxStateChanged(int state);
  void removeZeroValueCheckBoxStateChanged(int state);
  void savitzkyGolayGroupBoxClicked(bool);
  void sgValueChanged(int value);
  QString paramsAsText() const;

  signals:
  void mzIntegrationParamsChangedSignal(MzIntegrationParams params);
};

} // namespace msXpSmineXpert
