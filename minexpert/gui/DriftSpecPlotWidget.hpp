/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QColor>
#include <QScriptValue>


/////////////////////// QCustomPlot
#include <minexpert/gui/AbstractPlotWidget.hpp>
#include <minexpert/gui/AbstractMultiPlotWnd.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>
#include <minexpert/nongui/History.hpp>

///////////////////////////// Local includes


namespace msXpSmineXpert
{


//! The DriftSpecPlotWidget class provides a drift spectrum-specific plot
//! widget.
class DriftSpecPlotWidget : public AbstractPlotWidget
{
  Q_OBJECT

  public:
  // Construction/Desctruction
  DriftSpecPlotWidget(QWidget *parent,
                      const QString &name,
                      const QString &desc,
                      const MassSpecDataSet *massSpecDataSet = Q_NULLPTR,
                      const QString &fileName                = QString(),
                      bool isMultiGraph                      = false);

  ~DriftSpecPlotWidget();

  int arbitraryIntegrationType();

  QString craftAnalysisStanza(QCPGraph *graph         = Q_NULLPTR,
                              const QString &fileName = QString());

  void mouseReleaseHandledEvent(QMouseEvent *event);
  void keyReleaseEvent(QKeyEvent *event);

  Q_INVOKABLE void jsSetMzIntegrationParams();
  Q_INVOKABLE QScriptValue jsMzIntegrationParams() const;

  void integrateToMz(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE void jsIntegrateToMz(double lower = qSNaN(),
                                   double upper = qSNaN());

  void integrateToRt(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE void jsIntegrateToRt(double lower = qSNaN(),
                                   double upper = qSNaN());

  void integrateToTicIntensity(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE double jsIntegrateToTicIntensity(double lower = qSNaN(),
                                               double upper = qSNaN());

  Q_INVOKABLE void newPlot();

  signals:
  void recordAnalysisStanza(QString stanza, const QColor &color = QColor());

  void newMassSpectrum(const MassSpecDataSet *massSpecDataSet,
                       const QString &msg,
                       const History &history,
                       QColor color);

  void jsNewMassSpectrum(const MassSpecDataSet *massSpecDataSet,
                         const QString &msg,
                         const History &history,
                         QColor color);

  void newTicChromatogram(const MassSpecDataSet *massSpecDataSet,
                          const QString &msg,
                          const History &history,
                          QColor color);

  void jsNewTicChromatogram(const MassSpecDataSet *massSpecDataSet,
                            const QString &msg,
                            const History &history,
                            QColor color);

  void ticIntensitySignal(const MassSpecDataSet *massSpecDataSet,
                          const QString &msg,
                          const History &history);
};


} // namespace msXpSmineXpert
