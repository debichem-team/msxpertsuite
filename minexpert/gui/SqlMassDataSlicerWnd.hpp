/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>


/////////////////////// Local includes
#include "ui_SqlMassDataSlicerWnd.h"
#include <minexpert/nongui/SqlMassDataSlicer.hpp>
#include <minexpert/nongui/globals.hpp>

namespace msXpSmineXpert
{


//! The SqlMassDataSlicerWnd class provides an iterface to configure the slicing
//! of files.
class SqlMassDataSlicerWnd : public QMainWindow
{
  Q_OBJECT

  private:
  //! Graphical user interface definition.
  Ui::SqlMassDataSlicerWnd m_ui;

  //! Slicer object
  SqlMassDataSlicer m_slicer;

  //! Sqlite3 database data file handler
  MassSpecSqlite3Handler m_sqlHandler;

  //! User widget's history placeholder
  History m_history;

  //! Name of the application to display in the window title.
  QString m_applicationName;

  public:
  SqlMassDataSlicerWnd(QWidget *parent, const QString &applicationName);
  ~SqlMassDataSlicerWnd();

  void initialize(QString fileName,
                  double rangeStart,
                  double rangeEnd,
                  const History &history);
  void activate();
  Q_INVOKABLE void setSrcFileName(const QString &fileName);
  Q_INVOKABLE bool setDestDirectory(QString destDir);

  Q_INVOKABLE bool setFormatString(QString formatString);
  void setHistory(const History &history);

  Q_INVOKABLE void setRangeStart(double start);
  Q_INVOKABLE double rangeStart();

  Q_INVOKABLE void setRangeEnd(double end);
  Q_INVOKABLE double rangeEnd();

  Q_INVOKABLE void setRange(double start, double end);

  Q_INVOKABLE bool setSliceCount(int count);
  Q_INVOKABLE bool setSliceSize(double size);
  Q_INVOKABLE bool setExportToOneFile();

  void readSettings();
  void writeSettings();

  Q_INVOKABLE QString fileNames();
  Q_INVOKABLE QString reviewFileNames();

  void updateFeedback(QString msg,
                      int currentValue,
                      bool setVisible,
                      int startValue = -1,
                      int endValue   = -1);

  void updateSliceFileName(QString fileName);

  Q_INVOKABLE void validate(bool autoConfirm = false);
  Q_INVOKABLE QString confirm();
  Q_INVOKABLE void run();

  void destDirectoryPushButton();
  void displayedRegionCheckBoxStateChanged(int checkState);
  void sliceRadioButtonToggled(bool checked);
};

} // namespace msXpSmineXpert
