/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QPushButton>


/////////////////////// Local includes
#include <minexpert/nongui/globals.hpp>
#include <minexpert/gui/TicChromWnd.hpp>
#include <minexpert/gui/Application.hpp>
#include <minexpert/gui/MainWindow.hpp>
#include <libmass/MsMultiHash.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderPwiz.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderDx.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderMs1.hpp>
#include <minexpert/nongui/MassDataIntegrator.hpp>
#include <minexpert/nongui/MassSpecDataStats.hpp>


#include <omp.h>

namespace msXpSmineXpert
{


int type = qRegisterMetaType<QVector<double>>("QVector<double>");

//! Initialize the widget count to 0.
int TicChromWnd::widgetCounter = 0;


//! Construct an initialized TicChromWnd instance.
/*!

  This function calls initialize() to perform the initialization of the
  widgets.

  \param parent parent widget.

*/
TicChromWnd::TicChromWnd(QWidget *parent)
  : AbstractMultiPlotWnd{
      parent, "mineXpert" /*m_name*/, "TIC-chromatogram" /*m_desc*/}
{
  initialize();
}


//! Destruct \c this TicChromWnd instance.
TicChromWnd::~TicChromWnd()
{
  writeSettings();
}


//! Initialize the various widgets. Calls initializePlotRegion().
void
TicChromWnd::initialize()
{
  initializePlotRegion();
  readSettings();
}


//! Perform the initialization of the plot region of the window.
void
TicChromWnd::initializePlotRegion()
{
  // There are two regions in the window. The upper part of the mp_splitter
  // is gonna receive the multi-graph plotwidget. The lower part of the
  // splitter will receive all the individual plot widgets.

  // We will need later to know that this plot widget is actually for
  // displaying many different graphs, contrary to the normal situation. See
  // the true parameter in the function call below.

  mp_multiGraphPlotWidget =
    new TicChromPlotWidget(this,
                           m_name,
                           m_desc,
                           Q_NULLPTR /* MassSpecDataSet * */,
                           QString() /* fileName */,
                           true /* isMultiGraph */);
  mp_multiGraphPlotWidget->setObjectName("multiGraphPlot");


  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Created new multigraph TIC plot widget:"
  //<< mp_multiGraphPlotWidget << "window:" << this;

  // We need to remove the single graph that was created upon creation of
  // the QCustomPlot widget because we'll later add graphs, and we want to
  // have a proper correlation between the index of the graphs and their
  // pointer value. If we did not remove that graph "by default", then there
  // would be a gap of 1 between the index of the first added mass graph and
  // its expected value.
  mp_multiGraphPlotWidget->clearGraphs();

  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.
  connect(this,
          &TicChromWnd::replotWithAxisRange,
          mp_multiGraphPlotWidget,
          &TicChromPlotWidget::replotWithAxisRange);

  // This window is responsible for the writing of the analysis stanzas to
  // the file. We need to catch the signals.
  connect(mp_multiGraphPlotWidget,
          &TicChromPlotWidget::recordAnalysisStanza,
          this,
          &TicChromWnd::recordAnalysisStanza);

  // This window is responsible for displaying the key/value pairs of the
  // graphs onto which the mouse is moved. We need to catch the signal.
  connect(mp_multiGraphPlotWidget,
          &AbstractPlotWidget::updateStatusBarSignal,
          this,
          &AbstractMultiPlotWnd::updateStatusBar);

  // We want to create a scroll area in which the multigraph plot widget
  // will be sitting.
  QScrollArea *scrollArea = new QScrollArea();
  scrollArea->setObjectName(QStringLiteral("scrollArea"));
  scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  // We accept that the plots are resized, but we set a minimum size when we
  // create them.
  scrollArea->setWidgetResizable(true);
  scrollArea->setAlignment(Qt::AlignCenter);
  scrollArea->setWidget(mp_multiGraphPlotWidget);
  // And now prepend to the mp_splitter that new scroll area. Remember that
  // the mp_splitter had already a widget set to it, that is, the scroll
  // area that is now located below the mp_multiGraphPlotWidget, that is
  // responsible for the hosting of all the single-graph plot widgets.
  mp_splitter->insertWidget(0, scrollArea);
}


//! Add a new plot widget.
/*!

  \param keyVector vector of double values to be used as the x-axis data.

  \param valVector vector of double values to be used as the y-axis data.

  \param desc string containg the description of the plot.

  \param massSpecDataSet the MassSpecDataSet instance in which the mass data
  have been stored in the first place.

  \param isMultiGraph tell if the plot widget will be plot widget into which
  multiple graph will be plotted.

  \return the allocated plot widget.

*/
TicChromPlotWidget *
TicChromWnd::addPlotWidget(const QVector<double> &keyVector,
                           const QVector<double> &valVector,
                           const QString &desc,
                           const QColor &color,
                           const MassSpecDataSet *massSpecDataSet,
                           bool isMultiGraph)
{
  MainWindow *mainWindow = static_cast<MainWindow *>(parent());

  // Beware that massSpecDataSet might be Q_NULLPTR.
  QString fileName;
  if(massSpecDataSet != Q_NULLPTR)
    fileName = massSpecDataSet->fileName();

  TicChromPlotWidget *widget = new TicChromPlotWidget(
    this, m_name, desc, massSpecDataSet, fileName, isMultiGraph);

  widget->setObjectName("monoGraphPlot");


  ///////////////// BEGIN Script availability of the new widget
  /////////////////////

  // Craft a name that differentiates this widget from the previous and next
  // ones. We use the static counter number to affix to the string
  // "ticChrom".

  QString propName =
    QString("ticChromPlotWidget%1").arg(TicChromWnd::widgetCounter);
  QString alias = "lastTicChromPlotWidget";

  QString explanation =
    QString(
      "Make available a TIC chromatogram plot widget under name \"%1\" "
      "with alias \"%2\".\n")
      .arg(propName)
      .arg(alias);

  // Make the new plot widget globally available under the crafted property
  // name and alias. Also, put out the explanation text. Note that by
  // providing 'this', we make the new object appear in the scripting objet
  // tree widget as a child item of this.

  QScriptValue scriptValue;
  mainWindow->mp_scriptingWnd->publishQObject(
    widget,
    this /* parent object */,
    propName,
    alias,
    explanation,
    QString() /* help string */,
    scriptValue,
    QScriptEngine::QtOwnership,
    QScriptEngine::QObjectWrapOptions());

  ++TicChromWnd::widgetCounter;

  ///////////////// END Script availability of the new widget //////////////////


  widget->setMinimumSize(400, 150);
  mp_scrollAreaVBoxLayout->addWidget(widget);

  widget->setGraphData(keyVector, valVector);

  widget->setPlottingColor(color);

  widget->rescaleAxes(true /*only enlarge*/);
  widget->replot();

  // Store in the history the new ranges of both x and y axes.
  widget->updateAxisRangeHistory();

  // At this point, make sure that the same data are represented on the
  // multi-graph plot widget.

  QPen currentPen    = widget->graph()->pen();
  QCPGraph *newGraph = mp_multiGraphPlotWidget->addGraph();
  newGraph->setData(keyVector, valVector);

  // Duplicate the same widget pen for the multigraph graph.
  newGraph->setPen(widget->graph()->pen());
  mp_multiGraphPlotWidget->rescaleAxes(true /*only enlarge*/);
  mp_multiGraphPlotWidget->replot();

  widget->setMultiGraph(newGraph);

  // Maintain a hash connection between any given plot widget and the
  // corresponding graph in the multigraph widget.
  m_plotWidgetGraphMap.insert(widget, newGraph);

  // Finally append the new widget to the list of widgets that belong to
  // this window.
  m_plotWidgetList.append(widget);

  // Signal/slot connections.

  connect(widget,
          &TicChromPlotWidget::recordAnalysisStanza,
          this,
          &TicChromWnd::recordAnalysisStanza);

  connect(widget,
          &AbstractPlotWidget::updateStatusBarSignal,
          this,
          &AbstractMultiPlotWnd::updateStatusBar);

  connect(this,
          &AbstractMultiPlotWnd::showHelpSummarySignal,
          widget,
          &TicChromPlotWidget::showHelpSummary);

  return widget;
}


void
TicChromWnd::initialTic(const MassSpecDataSet *p_massSpecDataSet, QColor color)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "() - enter"
  //<< " - current thread id:" << QThread::currentThreadId()
  //<< "with color:" << color;

  if(p_massSpecDataSet == Q_NULLPTR)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Pointer cannot be nullptr."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  mp_statusBarCancelPushButton->setVisible(true);

  // QThread will start the thread in which the integrator will work.
  QThread *thread = new QThread;

  // The integrator will perform the integration itself.
  MassDataIntegrator *integrator = new MassDataIntegrator(p_massSpecDataSet);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Allocated new integrator:" << integrator;

  integrator->moveToThread(thread);

  // Calling thread->start (QThread::start()) will automatically call the
  // integrateToMz() function of the integrator.
  connect(thread, SIGNAL(started()), integrator, SLOT(initialTic()));

  // When the integration is finished it emits resultsReadySignal and thus
  // we can stop the *real* thread in the QThread thread instance, using the
  // signal/slot mechanism. That is not like calling QThead::quit() !!!
  connect(integrator, SIGNAL(resultsReadySignal()), thread, SLOT(quit()));

  // This will be needed later.
  connect(integrator,
          &MassDataIntegrator::resultsReadySignal,
          this,
          [integrator, color, this]() { initialTicDone(integrator, color); });

  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

  connect(integrator,
          &MassDataIntegrator::updateFeedbackSignal,
          this,
          &AbstractMultiPlotWnd::updateFeedback);

  connect(this,
          &AbstractMultiPlotWnd::cancelOperationSignal,
          integrator,
          &MassDataIntegrator::cancelOperation);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< " - current thread id:" << QThread::currentThreadId()
  //<< "going to thread->start()";

  thread->start();
}


void
TicChromWnd::initialTicDone(MassDataIntegrator *integrator, QColor color)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "() - enter"
  //<< " - current thread id:" << QThread::currentThreadId()
  //<< "with color:" << color
  //<< "with integrator:" << integrator
  //<< "and this:" << this;

  // Immediately get pointers and objects out of integrator that we'll
  // delete soon.
  const MassSpecDataSet *massSpecDataSet = integrator->massSpecDataSet();
  History localHistory                   = integrator->history();

  // De-setup the feedback.
  QTimer::singleShot(2000, this, [=]() {
    updateFeedback("", -1, false /* setVisible */, LogType::LOG_TO_STATUS_BAR);
  });

  // Make sure the cancel button is hidden.
  mp_statusBarCancelPushButton->setVisible(false);

  // At this point what we should do is transfer the data to the plotting
  // widget creation routine. Note that we are creating the very first plot
  // widget with data just loaded from a mass spec file. That plot widget
  // will be later connected to the destruction of the mass spec data set
  // when the plot widget itself is deleted (see true param in newRelation
  // call below).

  AbstractPlotWidget *widget = addPlotWidget(integrator->keyVector(),
                                             integrator->valVector(),
                                             massSpecDataSet->fileName(),
                                             color,
                                             massSpecDataSet);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "created a new widget for the tic chromatogram:" << widget
  //<< "with massSpecDataSet:" << massSpecDataSet;

  // Finally we can delete the integrator
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Finally deleting the integrator object:" << integrator;

  delete integrator;
  integrator = Q_NULLPTR;

  // For this specific TIC chrom widget, that is created for storing the
  // initial mass spec data set as read from file, we set it as parent of
  // the mass spec data set, so that when the widget is destroyed, the mass
  // spec data set also. Ugly, but we need to cast to non-const.
  const_cast<MassSpecDataSet *>(massSpecDataSet)->setParent(widget);

  // Before updating the history of the widget, update the history with the
  // two critical data bits that will be conditioning the way future
  // integrations are performed: the smallestStep value that goes along the
  // msXpS::MassToleranceType::MASS_TOLERANCE_MZ.

  MassSpecDataStats stats = massSpecDataSet->statistics();

  MzIntegrationParams paramsToApply;

  if(stats.m_spectrumCount <= 1)
    {
      // There is no interest to perform any binning when there is at most one
      // spectrum.

      MzIntegrationParams params(BinningType::BINNING_TYPE_NONE,
                                 0.005,
                                 msXpS::MassToleranceType::MASS_TOLERANCE_MZ,
                                 false /* apply mz shift */,
                                 false /* remove 0-val data points */);

      paramsToApply = params;
    }
  else
    {
      // Use the statistics to setup the initial mz integration params to the
      // history of the plot widget. Note that we set the binning type to
      // arbitrary with the bin size set to twice the smallest m/z step observed
      // throughout all the data points of all the spectra of the file.

      MzIntegrationParams params(
        BinningType::BINNING_TYPE_ARBITRARY,

        // This was the first version, but that value is not robuts: many
        // times it happens to be 0, so the 0.001 default value would be set.
        // That m_smallestStep value is too low for most occasions.
        // m_smallestStep,

        // This was the second version:
        // The m_smallestStepAvg is the most robust.
        // stats.m_smallestStepAvg,

        // This is currently the last version
        // The m_smallestMedian is the most robust.
        stats.m_smallestStepMedian,
        msXpS::MassToleranceType::MASS_TOLERANCE_MZ,
        false /* apply mz shift */,

        // Note that when binning is set, then removing the 0-val data points
        // is recommended.
        true /* remove 0-val data points */);

      paramsToApply = params;
    }

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "stats.m_smallestStepAvg =" << stats.m_smallestStepAvg;

  localHistory.setMzIntegrationParams(paramsToApply);

  // At this point, make sure that the widget has the new version of the
  // history.

  widget->rhistory().copyHistory(localHistory, true /*remove duplicates*/);

  // Log the history to the console.
  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(widget->history().innermostRangesAsText());

  // qDebug() << __FILE__ << __LINE__
  //<< "After initialTic was computed, history:"
  //<< widget->history().asText();

  mainWindow->logConsoleMessage(stats.asText(), color);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Statistics: " << massSpecDataSet->statisticsAsText();

  // The plot widget reacts to the Qt::Key_Delete key by calling a function
  // that emits a set of signals, of which the destroyPlotWidget signal
  // actually indirectly triggers the destruction of the plot widget. This
  // signal is now connected to the main window where a function will call
  // for the destruction of all the target plot widgets, that is all the
  // plot widgets that were created as the result of an integration
  // computation of the plot widget currently being destroyed.

  connect(widget,
          &AbstractPlotWidget::destroyPlotWidget,
          mainWindow,
          &MainWindow::destroyAlsoAllTargetPlotWidgets);

  connect(widget,
          &AbstractPlotWidget::toggleMultiGraphSignal,
          mainWindow,
          &MainWindow::toggleMultiGraph);

  // Ensure that there is a connexion between this new widget and the
  // MassSpecWnd instance that will receive the signal requiring to make a
  // mass spectrum on the basis of an integration selection in the TIC
  // chromatogram plot.

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newMassSpectrum,
          mainWindow->mp_massSpecWnd,
          &MassSpecWnd::newMassSpectrum);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newDriftSpectrum,
          mainWindow->mp_driftSpecWnd,
          &DriftSpecWnd::newDriftSpectrum);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newTicChromatogram,
          this,
          &TicChromWnd::newTicChromatogram);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::ticIntensitySignal,
          this,
          &TicChromWnd::ticIntensity);

  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.
  connect(this,
          &TicChromWnd::replotWithAxisRange,
          widget,
          &TicChromPlotWidget::replotWithAxisRange);

  // Make sure we get the signal requiring to redraw the background of the
  // widget, depending on it being focused or not.
  connect(this,
          &AbstractMultiPlotWnd::redrawPlotBackground,
          widget,
          &AbstractPlotWidget::redrawPlotBackground);

  // Set this newly created widget to be the last-focused widget.
  widget->setFocus();
  mp_lastFocusedPlotWidget = widget;
  updateMzIntegrationParams(widget->history().mzIntegrationParams());

  // Now call the main window of the program to finalize the widget
  // establishment.

  mainWindow->initialTicDone(widget, massSpecDataSet, color);
}


void
TicChromWnd::jsInitialTic(const MassSpecDataSet *p_massSpecDataSet,
                          QColor color)
{

  if(p_massSpecDataSet == Q_NULLPTR)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Pointer cannot be nullptr."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  // The mass data integration processes are handled by the
  // MassDataIntegrator object.

  MassDataIntegrator integrator(p_massSpecDataSet);

  integrator.initialTic();

  History localHistory = integrator.history();

  // At this point what we should do is transfer the data to the plotting
  // widget creation routine. Note that we are creating the very first plot
  // widget with data just loaded from a mass spec file. That plot widget
  // will be later connected to the destruction of the mass spec data set
  // when the plot widget itself is deleted (see true param in newRelation
  // call below).

  AbstractPlotWidget *widget = addPlotWidget(integrator.keyVector(),
                                             integrator.valVector(),
                                             p_massSpecDataSet->fileName(),
                                             color,
                                             p_massSpecDataSet);

  // For this specific TIC chrom widget, that is created for storing the
  // initial mass spec data set as read from file, we set it as parent of
  // the mass spec data set, so that when the widget is destroyed, the mass
  // spec data set also. Ugly, but we need to cast to non-const.
  const_cast<MassSpecDataSet *>(p_massSpecDataSet)->setParent(widget);

  // Before updating the history of the widget, update the history with the
  // two critical data bits that will be conditioning the way future
  // integrations are performed: the smallestStep value that goes along the
  // msXpS::MassToleranceType::MASS_TOLERANCE_MZ.

  MassSpecDataStats stats = p_massSpecDataSet->statistics();

  MzIntegrationParams paramsToApply;

  if(stats.m_spectrumCount <= 1)
    {
      // There is no interest to perform any binning when there is at most one
      // spectrum.

      MzIntegrationParams params(BinningType::BINNING_TYPE_NONE,
                                 0.005,
                                 msXpS::MassToleranceType::MASS_TOLERANCE_MZ,
                                 false /* apply mz shift */,
                                 false /* remove 0-val data points */);

      paramsToApply = params;
    }
  else
    {
      // Use the statistics to setup the initial mz integration params to the
      // history of the plot widget. Note that we set the binning type to
      // arbitrary with the bin size set to twice the smallest m/z step observed
      // throughout all the data points of all the spectra of the file.

      MzIntegrationParams params(
        BinningType::BINNING_TYPE_ARBITRARY,

        // This was the first version, but that value is not robuts: many
        // times it happens to be 0, so the 0.001 default value would be set.
        // That m_smallestStep value is too low for most occasions.
        // m_smallestStep,

        // This was the second version:
        // The m_smallestStepAvg is the most robust.
        // stats.m_smallestStepAvg,

        // This is currently the last version
        // The m_smallestMedian is the most robust.
        stats.m_smallestStepMedian,
        msXpS::MassToleranceType::MASS_TOLERANCE_MZ,
        false /* apply mz shift */,

        // Note that when binning is set, then removing the 0-val data points
        // is recommended.
        true /* remove 0-val data points */);

      paramsToApply = params;
    }

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "stats.m_smallestStepAvg =" << stats.m_smallestStepAvg;

  localHistory.setMzIntegrationParams(paramsToApply);

  // At this point, make sure that the widget has the new version of the
  // history.

  widget->rhistory().copyHistory(localHistory, true /*remove duplicates*/);

  // Log the history to the console.
  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(widget->history().innermostRangesAsText());

  // qDebug() << __FILE__ << __LINE__
  //<< "After initialTic was computed, history:"
  //<< widget->history().asText();

  mainWindow->logConsoleMessage(stats.asText(), color);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Statistics: " << p_massSpecDataSet->statisticsAsText();

  // The plot widget reacts to the Qt::Key_Delete key by calling a function
  // that emits a set of signals, of which the destroyPlotWidget signal
  // actually indirectly triggers the destruction of the plot widget. This
  // signal is now connected to the main window where a function will call
  // for the destruction of all the target plot widgets, that is all the
  // plot widgets that were created as the result of an integration
  // computation of the plot widget currently being destroyed.

  connect(widget,
          &AbstractPlotWidget::destroyPlotWidget,
          mainWindow,
          &MainWindow::destroyAlsoAllTargetPlotWidgets);

  connect(widget,
          &AbstractPlotWidget::toggleMultiGraphSignal,
          mainWindow,
          &MainWindow::toggleMultiGraph);

  // Ensure that there is a connexion between this new widget and the
  // MassSpecWnd instance that will receive the signal requiring to make a
  // mass spectrum on the basis of an integration selection in the TIC
  // chromatogram plot.

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newMassSpectrum,
          mainWindow->mp_massSpecWnd,
          &MassSpecWnd::newMassSpectrum);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newDriftSpectrum,
          mainWindow->mp_driftSpecWnd,
          &DriftSpecWnd::newDriftSpectrum);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::newTicChromatogram,
          this,
          &TicChromWnd::newTicChromatogram);

  connect(static_cast<TicChromPlotWidget *>(widget),
          &TicChromPlotWidget::ticIntensitySignal,
          this,
          &TicChromWnd::ticIntensity);


  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.
  connect(this,
          &TicChromWnd::replotWithAxisRange,
          widget,
          &TicChromPlotWidget::replotWithAxisRange);

  // Make sure we get the signal requiring to redraw the background of the
  // widget, depending on it being focused or not.
  connect(this,
          &AbstractMultiPlotWnd::redrawPlotBackground,
          widget,
          &AbstractPlotWidget::redrawPlotBackground);

  // Set this newly created widget to be the last-focused widget.
  widget->setFocus();
  mp_lastFocusedPlotWidget = widget;
  updateMzIntegrationParams(widget->history().mzIntegrationParams());


  // Now call the main window of the program to finalize the widget
  // establishment.
  mainWindow->initialTicDone(widget, p_massSpecDataSet, color);
}


//! Integrate data to TIC chromatogram.
/*!

  \param massSpecDataSet pointer to the MassSpecDataSet where the mass data
  are stored.

  \param integrationType IntegrationType of the integration to be performed.

  \param history History to account for the integration.

  \param color to use to plot the graph of the new mass spectrum.

*/
void
TicChromWnd::newTicChromatogram(const MassSpecDataSet *massSpecDataSet,
                                const QString &msg,
                                History history,
                                QColor color)
{

  if(massSpecDataSet == Q_NULLPTR)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Pointer cannot be nullptr."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);


  // Plot widget relation between the caller widget and the newly created
  // widget later on.
  AbstractPlotWidget *senderPlotWidget =
    static_cast<AbstractPlotWidget *>(QObject::sender());

  mp_statusBarCancelPushButton->setVisible(true);

  // QThread will start the thread in which the integrator will work.
  QThread *thread = new QThread;

  // The integrator will perform the integration itself.
  MassDataIntegrator *integrator =
    new MassDataIntegrator(massSpecDataSet, history);

  integrator->moveToThread(thread);

  // Calling thread->start (QThread::start()) will automatically call the
  // integrateToMz() function of the integrator.
  connect(thread, SIGNAL(started()), integrator, SLOT(integrateToRt()));

  // When the integration is finished it emits resultsReadySignal and thus
  // we can stop the *real* thread in the QThread thread instance, using the
  // signal/slot mechanism. That is not like calling QThead::quit() !!!
  connect(integrator, SIGNAL(resultsReadySignal()), thread, SLOT(quit()));

  // This will be needed later, when we'll create a data plot relation
  // object relating the sender plot widget with the newly created one (in
  // newTicChromatogramDone).
  connect(integrator,
          &MassDataIntegrator::resultsReadySignal,
          this,
          [integrator, senderPlotWidget, color, this]() {
            newTicChromatogramDone(integrator, senderPlotWidget, color);
          });

  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

  connect(integrator,
          &MassDataIntegrator::updateFeedbackSignal,
          this,
          &AbstractMultiPlotWnd::updateFeedback);

  connect(this,
          &AbstractMultiPlotWnd::cancelOperationSignal,
          integrator,
          &MassDataIntegrator::cancelOperation);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< " - current thread id:" << QThread::currentThreadId();

  thread->start();
}


void
TicChromWnd::newTicChromatogramDone(
  msXpSmineXpert::MassDataIntegrator *integrator,
  msXpSmineXpert::AbstractPlotWidget *senderPlotWidget,
  QColor color)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()";

  // De-setup the feedback.
  QTimer::singleShot(2000, this, [=]() {
    updateFeedback("", -1, false /* setVisible */, LogType::LOG_TO_STATUS_BAR);
  });

  // Make sure the cancel button is hidden.
  mp_statusBarCancelPushButton->setVisible(false);

  // Create a text item to indicate the last history item.

  QString historyItemText =
    integrator->history().newestHistoryItem()->asText(true /* brief */);

  QString plotText = QString("%1\n%2 - %3 ")
                       .arg(integrator->massSpecDataSet()->fileName())
                       //.arg(m_desc)
                       .arg("XIC chrom.")
                       .arg(historyItemText);

  // Allocate the new plot widget in which the data are to be plot.

  TicChromPlotWidget *widget = static_cast<TicChromPlotWidget *>(
    addPlotWidget(integrator->keyVector(),
                  integrator->valVector(),
                  plotText,
                  color,
                  integrator->massSpecDataSet()));

  // The history that we get as a parameter needs to be appended to the new
  // widget's history so that we have a full account of the history.

  widget->rhistory().copyHistory(integrator->history(),
                                 true /*remove duplicates*/);

  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(widget->history().asText(
    "Plot widget history: \n", "End plot widget history:\n"));
  // And now only the innermost ranges of the whole history.
  mainWindow->logConsoleMessage(widget->history().innermostRangesAsText());

  // The plot widget reacts to the Qt::Key_Delete key by calling a function
  // that emits a set of signals, of which the destroyPlotWidget signal
  // actually indirectly triggers the destruction of the plot widget. This
  // signal is now connected to the main window where a function will call for
  // the destruction of all the target plot widgets, that is all the plot
  // widgets that were created as the result of an integration computation of
  // the plot widget currently being destroyed.

  connect(widget,
          &AbstractPlotWidget::destroyPlotWidget,
          mainWindow,
          &MainWindow::destroyAlsoAllTargetPlotWidgets);

  connect(widget,
          &AbstractPlotWidget::toggleMultiGraphSignal,
          mainWindow,
          &MainWindow::toggleMultiGraph);

  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.

  connect(this,
          &AbstractMultiPlotWnd::replotWithAxisRange,
          widget,
          &AbstractPlotWidget::replotWithAxisRange);

  // Make sure we get the signal requiring to redraw the background of the
  // widget, depending on it being focused or not.

  connect(this,
          &AbstractMultiPlotWnd::redrawPlotBackground,
          widget,
          &AbstractPlotWidget::redrawPlotBackground);

  connect(widget,
          &TicChromPlotWidget::newMassSpectrum,
          mainWindow->mp_massSpecWnd,
          &MassSpecWnd::newMassSpectrum);

  connect(widget,
          &TicChromPlotWidget::newDriftSpectrum,
          mainWindow->mp_driftSpecWnd,
          &DriftSpecWnd::newDriftSpectrum);

  connect(widget,
          &TicChromPlotWidget::newTicChromatogram,
          this,
          &TicChromWnd::newTicChromatogram);

  connect(widget,
          &TicChromPlotWidget::ticIntensitySignal,
          mainWindow->mp_ticChromWnd,
          &TicChromWnd::ticIntensity);

  mainWindow->m_dataPlotWidgetRelationer.newRelation(
    integrator->massSpecDataSet(),
    senderPlotWidget,
    widget,
    Q_NULLPTR /* open spectra dialog list widget item */);

  // Finally we can destroy the integrator.
  delete integrator;
}


//! Integrate data to TIC chromatogram.
/*!

  \param massSpecDataSet pointer to the MassSpecDataSet where the mass data
  are stored.

  \param integrationType IntegrationType of the integration to be performed.

  \param history History to account for the integration.

  \param color to use to plot the graph of the new mass spectrum.

*/
void
TicChromWnd::jsNewTicChromatogram(AbstractPlotWidget *caller,
                                  const MassSpecDataSet *massSpecDataSet,
                                  const QString &msg,
                                  History history,
                                  QColor color)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Entering";

  if(massSpecDataSet == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  // Make sure the cancel button is visible.
  mp_statusBarCancelPushButton->setVisible(true);

  MassDataIntegrator integrator(massSpecDataSet, history);

  connect(&integrator,
          &MassDataIntegrator::updateFeedbackSignal,
          this,
          &AbstractMultiPlotWnd::updateFeedback);

  connect(this,
          &AbstractMultiPlotWnd::cancelOperationSignal,
          &integrator,
          &MassDataIntegrator::cancelOperation);

  integrator.integrate();

  // qDebug() << "At this point the integration is finished. Desetup the
  // feedback.";

  // De-setup the feedback.
  QTimer::singleShot(2000, this, [=]() {
    updateFeedback("", -1, false /* setVisible */, LogType::LOG_TO_STATUS_BAR);
  });

  // Make sure the cancel button is hidden.
  mp_statusBarCancelPushButton->setVisible(false);

  // Create a text item to indicate the last history item.

  QString historyItemText =
    history.newestHistoryItem()->asText(true /* brief */);

  QString plotText = QString("%1\n%2 - %3 ")
                       .arg(massSpecDataSet->fileName())
                       // .arg(m_desc)
                       .arg("XIC chrom.")
                       .arg(historyItemText);

  // Allocate the new plot widget in which the data are to be plot.

  TicChromPlotWidget *widget =
    static_cast<TicChromPlotWidget *>(addPlotWidget(integrator.keyVector(),
                                                    integrator.valVector(),
                                                    plotText,
                                                    color,
                                                    massSpecDataSet));

  // The history that we get as a parameter needs to be appended to the new
  // widget's history so that we have a full account of the history.

  widget->rhistory().copyHistory(history, true /*remove duplicates*/);

  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(widget->history().asText(
    "Plot widget history: \n", "End plot widget history:\n"));
  // And now only the innermost ranges of the whole history.
  mainWindow->logConsoleMessage(widget->history().innermostRangesAsText());

  // The plot widget reacts to the Qt::Key_Delete key by calling a function
  // that emits a set of signals, of which the destroyPlotWidget signal
  // actually indirectly triggers the destruction of the plot widget. This
  // signal is now connected to the main window where a function will call for
  // the destruction of all the target plot widgets, that is all the plot
  // widgets that were created as the result of an integration computation of
  // the plot widget currently being destroyed.

  connect(widget,
          &AbstractPlotWidget::destroyPlotWidget,
          mainWindow,
          &MainWindow::destroyAlsoAllTargetPlotWidgets);

  connect(widget,
          &AbstractPlotWidget::toggleMultiGraphSignal,
          mainWindow,
          &MainWindow::toggleMultiGraph);

  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.

  connect(this,
          &AbstractMultiPlotWnd::replotWithAxisRange,
          widget,
          &AbstractPlotWidget::replotWithAxisRange);

  // Make sure we get the signal requiring to redraw the background of the
  // widget, depending on it being focused or not.

  connect(this,
          &AbstractMultiPlotWnd::redrawPlotBackground,
          widget,
          &AbstractPlotWidget::redrawPlotBackground);

  connect(widget,
          &TicChromPlotWidget::newMassSpectrum,
          mainWindow->mp_massSpecWnd,
          &MassSpecWnd::newMassSpectrum);

  connect(widget,
          &TicChromPlotWidget::newDriftSpectrum,
          mainWindow->mp_driftSpecWnd,
          &DriftSpecWnd::newDriftSpectrum);

  connect(widget,
          &TicChromPlotWidget::newTicChromatogram,
          this,
          &TicChromWnd::newTicChromatogram);

  mainWindow->m_dataPlotWidgetRelationer.newRelation(
    massSpecDataSet,
    caller,
    widget,
    Q_NULLPTR /* open spectra dialog list widget item */);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< "Exiting";
}


void
TicChromWnd::ticIntensity(const MassSpecDataSet *massSpecDataSet,
                          const QString &msg,
                          History history)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()";

  if(massSpecDataSet == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  mp_statusBarCancelPushButton->setVisible(true);

  AbstractPlotWidget *senderPlotWidget =
    static_cast<AbstractPlotWidget *>(QObject::sender());

  // QThread will start the thread in which the integrator will work.
  QThread *thread = new QThread;

  // The integrator will perform the integration itself.
  MassDataIntegrator *integrator =
    new MassDataIntegrator(massSpecDataSet, history);

  integrator->moveToThread(thread);

  // Calling thread->start (QThread::start()) will automatically call the
  // integrateToTicIntensity() function of the integrator.
  connect(
    thread, SIGNAL(started()), integrator, SLOT(integrateToTicIntensity()));

  // When the integration is finished it emits resultsReadySignal and thus
  // we can stop the *real* thread in the QThread thread instance, using the
  // signal/slot mechanism. That is not like calling QThead::quit() !!!
  connect(integrator, SIGNAL(resultsReadySignal()), thread, SLOT(quit()));

  // This will be needed later, when we'll create a data plot relation
  // object relating the sender plot widget with the newly created one (in
  // ticIntensityDone).
  connect(integrator,
          &MassDataIntegrator::resultsReadySignal,
          this,
          [integrator, msg, senderPlotWidget, this]() {
            ticIntensityDone(integrator, msg, senderPlotWidget);
          });

  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

  connect(integrator,
          &MassDataIntegrator::updateFeedbackSignal,
          this,
          &AbstractMultiPlotWnd::updateFeedback);

  connect(this,
          &AbstractMultiPlotWnd::cancelOperationSignal,
          integrator,
          &MassDataIntegrator::cancelOperation);

  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()"
  //<< " - current thread id:" << QThread::currentThreadId();

  thread->start();
}


void
TicChromWnd::ticIntensityDone(MassDataIntegrator *integrator,
                              QString msg,
                              AbstractPlotWidget *senderPlotWidget)
{
  // qDebug() << __FILE__ << __LINE__ << __FUNCTION__ << "()";

  // De-setup the feedback.
  QTimer::singleShot(2000, this, [=]() {
    updateFeedback("", -1, false /* isVisible */, LogType::LOG_TO_STATUS_BAR);
  });

  // Make sure the cancel button is hidden.
  mp_statusBarCancelPushButton->setVisible(false);

  // Set the proper value in the plot widget right away so that it can be
  // queried in another context.
  senderPlotWidget->setLastTicIntensity(integrator->ticIntensity());

  QString fullMsg =
    msg + QString(": TIC int.: %1").arg(integrator->ticIntensity());

  // Now we can dipslay the results in the status bar:
  statusBar()->showMessage(fullMsg);

  // And in the main console window, now , with the proper color.

  QColor color = senderPlotWidget->color();

  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(fullMsg, color, false /*overwrite*/);

  // Finally we can destroy the integrator.
  delete integrator;
}


void
TicChromWnd::newPlot(AbstractPlotWidget *senderPlotWidget,
                     const MassSpecDataSet *massSpecDataSet,
                     QVector<double> keys,
                     QVector<double> values,
                     const QString &msg,
                     History history,
                     QColor color)
{
  if(senderPlotWidget == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  if(massSpecDataSet == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  // Create a text item to indicate the last history item.

  QString historyItemText =
    history.newestHistoryItem()->asText(true /* brief */);

  QString plotText = QString("%1\n%2 - %3\n%4")
                       .arg(massSpecDataSet->fileName())
                       .arg(m_desc)
                       .arg(historyItemText)
                       .arg(msg);

  // Allocate the new plot widget in which the data are to be plot.

  TicChromPlotWidget *widget = static_cast<TicChromPlotWidget *>(
    addPlotWidget(keys, values, plotText, color, massSpecDataSet));

  // The history that we get as a parameter needs to be appended to the new
  // widget's history so that we have a full account of the history.

  widget->rhistory().copyHistory(history, true /*remove duplicates*/);

  MainWindow *mainWindow = static_cast<MainWindow *>(parent());
  mainWindow->logConsoleMessage(widget->history().asText(
    "Plot widget history: \n", "End plot widget history:\n"));
  // And now only the innermost ranges of the whole history.
  mainWindow->logConsoleMessage(widget->history().innermostRangesAsText());

  // The plot widget reacts to the Qt::Key_Delete key by calling a function
  // that emits a set of signals, of which the destroyPlotWidget signal
  // actually indirectly triggers the destruction of the plot widget. This
  // signal is now connected to the main window where a function will call for
  // the destruction of all the target plot widgets, that is all the plot
  // widgets that were created as the result of an integration computation of
  // the plot widget currently being destroyed.

  connect(widget,
          &AbstractPlotWidget::destroyPlotWidget,
          mainWindow,
          &MainWindow::destroyAlsoAllTargetPlotWidgets);

  connect(widget,
          &AbstractPlotWidget::toggleMultiGraphSignal,
          mainWindow,
          &MainWindow::toggleMultiGraph);

  // Ensure that there is a connection between this plot widget and this
  // containing window, such that if there is a requirement to replot, the
  // widget gets the signal.

  connect(this,
          &AbstractMultiPlotWnd::replotWithAxisRange,
          widget,
          &AbstractPlotWidget::replotWithAxisRange);

  // Make sure we get the signal requiring to redraw the background of the
  // widget, depending on it being focused or not.

  connect(this,
          &AbstractMultiPlotWnd::redrawPlotBackground,
          widget,
          &AbstractPlotWidget::redrawPlotBackground);

  mainWindow->m_dataPlotWidgetRelationer.newRelation(
    massSpecDataSet,
    senderPlotWidget,
    widget,
    Q_NULLPTR /* open spectra dialog list widget item */);
}


//! Toggle the visibility of the multigraph graph and the target widgets.
/*!

  The base class simply toggles the visibility of the multigraph graph
  corresponding to the signal emitter plot widget. This overridden function
  does more, not only does it toggle the visibility of the multigraph graph
  corresponding to the sender plot widget, but does the same for all the
  target plot widgets.

  This is useful in the case where there are many opened spectra in the
  program, thus many initial (top) TIC chromatogram plot widget that may have
  given rise to many mass spectra/drift spectra. It is sometimes useful to
  clear up the multigraph plot widgets in the mass and drift spectrum
  multigraph plot widgets by hiding/showing all the multigraph graphs relating
  to the sender plot widget.

*/
void
TicChromWnd::toggleMultiGraph()
{
  AbstractPlotWidget *ticChromPlotWidget =
    dynamic_cast<AbstractPlotWidget *>(QObject::sender());
  QCPGraph *graph = m_plotWidgetGraphMap.value(ticChromPlotWidget);

  if(graph == Q_NULLPTR)
    return;

  // First toggle visibility of this plot widget graph.
  if(graph->visible())
    graph->setVisible(false);
  else
    graph->setVisible(true);

  // And now ask for a replot of the multi-graph plot widget.
  mp_multiGraphPlotWidget->replot();

  // But we know that each tic chrom plot widget might have a number of mass
  // spec window plot widget that derive from it by way of mass spectrum
  // combination or also any number of drift spectra by the same integration
  // mechanism. All these plot widgets must be have
  // their visibility toggled also. That is, we need to get a recursive
  // search list of all the plot widget targets that were directly or
  // indirectly generated from this ticChromPlotWidget.

  MainWindow *mainWindow = static_cast<MainWindow *>(parent());

  QList<AbstractPlotWidget *> plotWidgetList;
  mainWindow->m_dataPlotWidgetRelationer.targetsRecursive(ticChromPlotWidget,
                                                          &plotWidgetList);

  for(int iter = 0; iter < plotWidgetList.size(); ++iter)
    {
      plotWidgetList.at(iter)->toggleMultiGraph();
    }
}


//! Script-invocable override of the base class function.
void
TicChromWnd::clearPlots()
{
  AbstractMultiPlotWnd::clearPlots();
}


//! Record an analysis stanza.
/*!

  \param stanza string to print out in the record.

  \param color color to use for the printout.

*/
void
TicChromWnd::recordAnalysisStanza(const QString &stanza, const QColor &color)
{
  // The record might go to a file or to the console or both.

  if((mp_analysisPreferences->m_recordTarget & RecordTarget::RECORD_TO_FILE) ==
     RecordTarget::RECORD_TO_FILE)
    {
      if(mp_analysisFile != Q_NULLPTR)
        {

          // Write the stanza, that was crafted by the calling plot widget to
          // the file.

          if(!mp_analysisFile->open(QIODevice::Append))
            {
              statusBar()->showMessage(
                QString("Could not record the step because "
                        "the file could not be opened."),
                4000);
            }
          else
            {
              mp_analysisFile->write(stanza.toLatin1());
              // Force writing because we may want to have tail -f work fine
              // on the
              // file, and see modifications live to change fiels in a text
              // editor.
              mp_analysisFile->flush();
              mp_analysisFile->close();
            }
        }
      else
        {
          qDebug() << __FILE__ << __LINE__
                   << "The mp_analysisFile pointer is Q_NULLPTR.";

          statusBar()->showMessage(
            QString("Could not record the analysis step to file. "
                    "Please define a file to write the data to."),
            4000);
        }
    }

  // Also, if recording to the console is asked for, then do that also.
  if((mp_analysisPreferences->m_recordTarget &
      RecordTarget::RECORD_TO_CONSOLE) == RecordTarget::RECORD_TO_CONSOLE)
    {
      MainWindow *mainWindow = static_cast<MainWindow *>(parent());
      mainWindow->logConsoleMessage(stanza, color);
    }

  // Also, if recording to the clipboard is asked for, then do that also.
  if((mp_analysisPreferences->m_recordTarget &
      RecordTarget::RECORD_TO_CLIPBOARD) == RecordTarget::RECORD_TO_CLIPBOARD)
    {
      QClipboard *clipboard = QApplication::clipboard();
      clipboard->setText(clipboard->text() + stanza, QClipboard::Clipboard);
    }

  return;
}


} // namespace msXpSmineXpert
