/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QTreeWidgetItem>

///////////////////////////// Local includes
#include <globals/globals.hpp>
#include <minexpert/gui/ScriptingWnd.hpp>


namespace msXpSmineXpert
{


class ScriptingObjectTreeWidgetItem : public QObject, QTreeWidgetItem
{

  friend ScriptingWnd;

  private:
  Q_OBJECT

  QObject *mp_qobject = Q_NULLPTR;
  QString m_comment;
  QString m_help;
  ScriptEntityType m_entityType;


  public:
  ScriptingObjectTreeWidgetItem(QTreeWidget *parent, int type);
  ScriptingObjectTreeWidgetItem(QTreeWidgetItem *parent, int type);
  ~ScriptingObjectTreeWidgetItem();

  void setQObject(QObject *object);
  void setComment(const QString &comment);
  void setHelp(const QString &help);
  void setScriptEntityType(ScriptEntityType type);

  void setProperties(
    QObject *object             = Q_NULLPTR,
    const QString &comment      = QString(),
    const QString &help         = QString(),
    ScriptEntityType entityType = ScriptEntityType::UndefinedEntity);

  public slots:

  void setText(int column, const QString &text);

  signals:

  public slots:
};

} // namespace msXpSmineXpert
