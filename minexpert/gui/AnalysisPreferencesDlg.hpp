/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>


/////////////////////// Local includes
#include "ui_AnalysisPreferencesDlg.h"
#include <minexpert/nongui/AnalysisPreferences.hpp>
#include <minexpert/nongui/globals.hpp>


namespace msXpSmineXpert
{


//! The AnalysisPreferencesDlg class provides an interface to the
//! AnalysisPreferences settings.
/*!

  The AnalysisPreferences are the settings that allow the user to configure
  how the various data mining actions are recorded to the console window, to
  the clipboard, to file.

  This dialog window class allows the user to configure the AnalysisPreferences.

  \sa AnalysisPreferences.
*/
class AnalysisPreferencesDlg : public QDialog
{
  Q_OBJECT

  protected:
  Ui::AnalysisPreferencesDlg m_ui;

  private:
  QString m_applicationName;

  QList<QPlainTextEdit *> m_plainTextEditList;

  AnalysisPreferences m_analysisPreferences;
  void initialize();

  void readSettings();
  void writeSettings();

  public:
  AnalysisPreferencesDlg(QWidget *parent, const QString &applicationName);
  ~AnalysisPreferencesDlg();

  AnalysisPreferences analysisPreferences();

  public slots:
  void openAnalysisFile();
  void addToHistoryPushButton();
  void removeFromHistoryPushButton();
  void okPushButton();
  void cancelPushButton();
  void historyItemActivated(int index);
};

} // namespace msXpSmineXpert
