/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef APPLICATION_HPP
#define APPLICATION_HPP


/////////////////////// Qt includes
#include <QtGui>
#include <QtXml>
#include <QtGlobal>
#include <QLocale>
#include <QSplashScreen>


/////////////////////// Local includes
#include <minexpert/nongui/globals.hpp>
#include <minexpert/gui/MainWindow.hpp>


namespace msXpSmineXpert
{

class Application : public QApplication
{
  Q_OBJECT

  private:
  private:
  QString m_moduleName = "minexpert";
  // The splash screen, that we'll remove automatically after
  // 2 seconds.
  QSplashScreen *mpa_splash;

  public:
  Application(int &argc, char **argv, const QString &moduleName);
  ~Application();

  Q_PROPERTY(
    QString m_description READ description WRITE setDescription USER true);
  QString m_description =
    QString("mineXpert: a mass spectrum viewer and analyzer");

  public slots:
  void setDescription(QString &desc);
  QString description();

  void destroySplash();
};

} // namespace msXpSmineXpert


#endif // APPLICATION_HPP
