/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QTreeWidget>
#include <QDebug>
#include <QMouseEvent>
#include <QMenu>
#include <QAction>


///////////////////////////// Local includes
#include <minexpert/gui/ScriptingObjectTreeWidgetItem.hpp>


namespace msXpSmineXpert
{


ScriptingObjectTreeWidgetItem::ScriptingObjectTreeWidgetItem(
  QTreeWidget *parent, int type)
  : QObject(parent), QTreeWidgetItem(parent, type)
{
}

ScriptingObjectTreeWidgetItem::ScriptingObjectTreeWidgetItem(
  QTreeWidgetItem *parent, int type)
  : QTreeWidgetItem(parent, type)
{
}


ScriptingObjectTreeWidgetItem::~ScriptingObjectTreeWidgetItem()
{
}


void
ScriptingObjectTreeWidgetItem::setQObject(QObject *object)
{
  if(object == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  mp_qobject = object;
}


void
ScriptingObjectTreeWidgetItem::setText(int column, const QString &text)
{
  QTreeWidgetItem::setText(column, text);
}


void
ScriptingObjectTreeWidgetItem::setComment(const QString &comment)
{
  m_comment = comment;
}


void
ScriptingObjectTreeWidgetItem::setHelp(const QString &help)
{
  m_help = help;
}


void
ScriptingObjectTreeWidgetItem::setScriptEntityType(ScriptEntityType type)
{
  m_entityType = type;
}


void
ScriptingObjectTreeWidgetItem::setProperties(QObject *object,
                                             const QString &comment,
                                             const QString &help,
                                             ScriptEntityType entityType)
{
  if(object != Q_NULLPTR)
    mp_qobject = object;

  m_comment = comment;
  m_help    = help;

  setToolTip(0, stanzifyParagraphs(m_help, 60));

  m_entityType = entityType;
}


} // namespace msXpSmineXpert
