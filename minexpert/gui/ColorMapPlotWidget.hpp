/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QColor>
#include <QScriptValue>


///////////////////////////// Local includes
#include <minexpert/gui/AbstractPlotWidget.hpp>
#include <minexpert/gui/AbstractMultiPlotWnd.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>
#include <minexpert/nongui/History.hpp>


namespace msXpSmineXpert
{

class ColorMapWnd;


//! The ColorMapPlotWidget class provides a specialized widget.
/*!

  The ColorMapPlotWidget provides a plot widget that is specialized for the
  plotting of color map data.

  When mobility ion mass spectrometry data are loaded from file, the program
  automatically computes the mz=f(dt) color map. The intensity of the mz,dt
  pair that is plotted in the color map is deduced from the color of the
  corresponding point. A datum element in the color map widget is thus
  characterized by the following\:

  - the key (that is in map jargon the x-axis value, the dt value);

  - the value (this is in map jargon the y-axis value, the mz value);

  - the intensity (that is the z value, that is actually color coded in the
  map).

  The ColorMapPlotWidget may be used as a starting point for the data mining
  process, as it is possible to trigger integrations starting from it to mass
  spectrum or to drift spectrum.

*/
class ColorMapPlotWidget : public AbstractPlotWidget
{
  Q_OBJECT;

  private:
  //! Tells if the keys of the color map are the mobility drift time.
  bool m_areDtKeys = true;

  public:
  ColorMapPlotWidget(QWidget *parent,
                     const QString &name,
                     const QString &desc,
                     const MassSpecDataSet *massSpecDataSet = nullptr,
                     const QString &fileName                = QString(),
                     bool isMultiGraph                      = false);
  ~ColorMapPlotWidget();

  int arbitraryIntegrationType();

  bool setupWidget();

  void setColorMapSizeAndRanges(int keySize,
                                int valSize,
                                QCPRange keyRange,
                                QCPRange valRange,
                                double fillingValue);

  void setColorMapPointData(double key, double val, double z);

  void rescaleColorMapDataRange(bool recalculateDataBounds = false);

  void setDecoratingColor(const QColor &color);

  void setColorMapGradient(QCPColorGradient colorGradient);

  void mouseReleaseHandledEvent(QMouseEvent *event);
  void keyReleaseEvent(QKeyEvent *event);
  void mousePressHandler(QMouseEvent *event);

  Q_INVOKABLE void jsSetMzIntegrationParams();
  Q_INVOKABLE QScriptValue jsMzIntegrationParams() const;

  void integrateToMz(double dtRangeStart,
                     double dtRangeEnd,
                     double mzRangeStart,
                     double mzRangeEnd);

  Q_INVOKABLE void jsIntegrateToMz(double dtRangeStart,
                                   double dtRangeEnd,
                                   double mzRangeStart,
                                   double mzRangeEnd);

  void integrateToDt(double dtRangeStart,
                     double dtRangeEnd,
                     double mzRangeStart,
                     double mzRangeEnd);

  Q_INVOKABLE void jsIntegrateToDt(double dtRangeStart,
                                   double dtRangeEnd,
                                   double mzRangeStart,
                                   double mzRangeEnd);

  void integrateToRt(double dtRangeStart,
                     double dtRangeEnd,
                     double mzRangeStart,
                     double mzRangeEnd);

  Q_INVOKABLE void jsIntegrateToRt(double dtRangeStart,
                                   double dtRangeEnd,
                                   double mzRangeStart,
                                   double mzRangeEnd);

  void integrateToTicIntensity(double dtRangeStart,
                               double dtRangeEnd,
                               double mzRangeStart,
                               double mzRangeEnd);

  Q_INVOKABLE double jsIntegrateToTicIntensity(double dtRangeStart,
                                               double dtRangeEnd,
                                               double mzRangeStart,
                                               double mzRangeEnd);

  Q_INVOKABLE void transposeAxes();


  signals:
  void recordAnalysisStanza(QString stanza, const QColor &color = QColor());

  void newMassSpectrum(const MassSpecDataSet *massSpecDataSet,
                       const QString &,
                       const History &history,
                       QColor color);

  void jsNewMassSpectrum(const MassSpecDataSet *massSpecDataSet,
                         const QString &,
                         const History &history,
                         QColor color);

  void newDriftSpectrum(const MassSpecDataSet *massSpecDataSet,
                        const QString &msg,
                        const History &history,
                        QColor color);

  void ticIntensitySignal(const MassSpecDataSet *massSpecDataSet,
                          const QString &msg,
                          const History &history);

  // No newTicChromatogram signal here because that signal is coded in
  // AbstractMultiPlotWidget, since it is emitted from all derived classes.
};


} // namespace msXpSmineXpert
