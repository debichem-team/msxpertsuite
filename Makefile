mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

all: 
	cd ${HOME}/devel/msxpertsuite/build-area && make 

.PHONY: clean
clean:
	cd ${HOME}/devel/msxpertsuite/build-area && make clean


