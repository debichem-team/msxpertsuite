/*  BEGIN software license
 *
 *  msXpertSuite - mass spectrometry software suite
 *  -----------------------------------------------
 *  Copyright(C) 2009, 2017 Filippo Rusconi
 *
 *  http://www.msxpertsuite.org
 *
 *  This file is part of the msXpertSuite project.
 *
 *  The msXpertSuite project is the successor of the massXpert project. This
 *  project now includes various independent modules:
 *
 *  - massXpert, model polymer chemistries and simulate mass spectrometric data;
 *  - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


////////////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QTextStream>
#include <QRegularExpression>

/*
   Bitwise stuff (from StackOverflow)

   It is sometimes worth using an enum to name the bits:

   enum ThingFlags = {
   ThingMask  = 0x0000,
   ThingFlag0 = 1 << 0,
   ThingFlag1 = 1 << 1,
   ThingError = 1 << 8,
   }

   Then use the names later on. I.e. write

   thingstate |= ThingFlag1;
   thingstate &= ~ThingFlag0;
   if (thing & ThingError) {...}

   to set, clear and test. This way you hide the magic numbers from the rest of
   your code.
   */


namespace msXpS
{


//! Type of user that makes use of resources.
/*!
 * When looking for resources, this enum variables allow defining if the
 * resources are to be searched in system directories or in the user $HOME
 * directory.
 */
enum UserType
{
  USER_TYPE_USER   = 0, //!< User is a logon user
  USER_TYPE_SYSTEM = 1  //!< User is the system
};

//! Type of mass.
/*!
 * Can be monoisotopic or average or both.
 */
enum MassType
{
  MASS_NONE = 0x0000,                //!< No mass
  MASS_MONO = 1 << 0,                //!< Monoisotopic mass
  MASS_AVG  = 1 << 1,                //!< Average mass
  MASS_BOTH = (MASS_MONO | MASS_AVG) //!< Both masses
};

//! Compression type of the mass data.
/*!
 * When loading mass spectra, defines if the data are compressed or not.
 *
 * \sa msXpSmineXpert::MassSpecDataFileLoaderPwiz
 * \sa msXpSmineXpert::MassSpecDataFileLoaderSqlite3
 * \sa msXpSlibmass::Base64EncDec
 */
enum DataCompression
{
  DATA_COMPRESSION_UNSET = -1,
  DATA_COMPRESSION_NONE  = 0,
  DATA_COMPRESSION_ZLIB  = 1,
};


//! Type of mass tolerance.
/*!
 * Can be atomic mass units, percentage or part per million.
 */
enum MassToleranceType
{
  MASS_TOLERANCE_NONE = 0,
  //!< parts per million
  MASS_TOLERANCE_PPM,
  //!< m/z ratio
  MASS_TOLERANCE_MZ,
  //!< atomic mass units
  MASS_TOLERANCE_AMU,
  //!< resolution, that is [1 / m/z]
  MASS_TOLERANCE_RES,
  MASS_TOLERANCE_LAST,
};

extern QMap<int, QString> massToleranceTypeMap;


//! Type of binning when performing integrations to a mass spectrum
enum BinningType
{
  //! < no binning
  BINNING_TYPE_NONE = 0,
  //! binning based on mass spectral data
  BINNING_TYPE_DATA_BASED,
  //! binning based on arbitrary bin size value
  BINNING_TYPE_ARBITRARY,
  BINNING_TYPE_LAST,
};

extern QMap<int, QString> binningTypeMap;


//! Kind of entity handled in a JavaScript object creation operation.
/*!
 * When creating JavaScript objects, define which kind of entity is being
 * created.
 *
 * \sa msXpSlibmassgui::ScriptingWnd
 * \sa msXpSlibmassgui::ScriptingObjectTreeWidgetItem
 */
enum ScriptEntityType
{
  UndefinedEntity,
  StringEntity,
  FunctionEntity,
  ArrayOrListEntity,
  QObjectEntity,
  IntegerEntity,
  DoubleEntity
};

extern QChar gConfigKeyStringPrefixDelimiter;

extern int FWHM_PEAK_SPAN_FACTOR;

extern int ATOM_DEC_PLACES;
extern int OLIGOMER_DEC_PLACES;
extern int POLYMER_DEC_PLACES;
extern int PH_PKA_DEC_PLACES;

extern QRegularExpression gXyFormatMassDataRegExp;
extern QRegularExpression gEndOfLineRegExp;

void doubleListStatistics(QList<double> list,
                          double *sum,
                          double *average,
                          double *variance,
                          double *stdDeviation,
                          double *nonZeroSmallest,
                          double *smallest,
                          double *greatest,
                          double *median);

bool almostEqual(double value1, double value2, int decimalPlaces = 10);

QTextStream &qStdOut();

QString &unspacifyString(QString &text);

QString binaryRepresentation(int value);

QString elideText(const QString &text,
                  int charsLeft            = 4,
                  int charsRight           = 4,
                  const QString &delimitor = "...");

QString stanzify(const QString &text, int width);

QString stanzifyParagraphs(const QString &text, int width);

int zeroDecimals(double value);

double ppm(double value, double ppm);

double addPpm(double value, double ppm);

double removePpm(double value, double ppm);

double res(double value, double res);

double addRes(double value, double res);

double removeRes(double value, double res);

QString pointerAsString(void *pointer);


} // namespace msXpS


Q_DECLARE_METATYPE(msXpS::BinningType);
Q_DECLARE_METATYPE(msXpS::MassToleranceType);

Q_ENUMS(msXpS::BinningType);
Q_ENUMS(msXpS::MassToleranceType);
