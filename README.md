msXpertSuite
============

msXpertSuite (http://www.msxpertsuite.org) is a collection of programs to simulate and analyse mass
spectrometric data obtained on linear (bio-)polymers. It is the successor of GNU
polyXmass, first, and of massXpert, second.

Two programs, massXpert, mineXpert allow the user to perform the following:

massXpert
---------

 - making brand new polymer chemistry definitions;
 - using the definitions to perform easy calculations in a desktop
   calculator-like manner;
 - performing sophisticated polymer sequence editing and simulations;
 - perform m/z list comparisons;

Chemical simulations encompass cleavage (either chemical or
enzymatic), gas-phase fragmentations, chemical modification of any
monomer in the polymer sequence, cross-linking of monomers in the
sequence, arbitrary mass searches, calculation of the isotopic
pattern...

mineXpert
---------

 - Open mass spectrometry data files (mzML, mzXML, asc, xy, ...);
 - Calculate and display the TIC chromatogram;
 - For mobility data, calculate and display a mz=f(dt) color map;
 - Integrate the data from the TIC chromatogram or color map
   - to mass spectrum;
   - to drift spectrum;
   - back to TIC chromatogram (XIC chromatogram);
   - reverse operations;
 - Export the data to text files;
 - Save plot to graphics files (pdf, png, jpg);
 - Slice a big initial file into smaller chunks for easier mining;
 - Define how mining activity is recorded on disk for later use;
 - Convert mzML files into a private (albeit open) mass spectrometry 
   format that allows better performance (based on SQLite3);
 - Scripting framework (JavaScript) allowing to process data and to
   display the new data as graphs in the graphical user interface.

