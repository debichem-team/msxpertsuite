<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN"
[
<!ENTITY progname "massxpert">
<!ENTITY progName "massXpert">
]>

<refentry>

	<refentryinfo>
		<date> 03 july 2017</date>
	</refentryinfo>

	<refmeta>
		<refentrytitle>
			<application>&progName</application>
		</refentrytitle>
		<manvolnum>1</manvolnum>
		<refmiscinfo>&progName; module of the msXpertSuite for mass spectrometry</refmiscinfo>
	</refmeta>

	<refnamediv>
		<refname>
			<application>&progName;</application>
		</refname>

		<refpurpose>
			Model linear polymers, simulate chemical reactions and predict mass spectrometric data
		</refpurpose>
	</refnamediv>

	<refsynopsisdiv>

		<cmdsynopsis>
			<command>&progname;</command>
			<arg><option><replaceable class="parameter">options</replaceable></option></arg>
			<arg><option><replaceable class="parameter">filename ...</replaceable></option></arg>
		</cmdsynopsis>

	</refsynopsisdiv>

	<refsect1>
		<title>DESCRIPTION</title>
		<para>

			This manual page documents briefly the
			<command>&progname;</command> software shipped within the
			<filename>msXpertSuite</filename> software suite.  

			This program allows one to
			model linear polymer chemistries using a dedicated grammar. Once the
			polymer chemistry of interest is defined, the user is allowed to use it to
			define polymer sequence files. These files serve as the starting point for
			the simulation of a large set of chemical reactions onto that polymer
			sequence. This software is comprised of four main submodules:

			<itemizedlist>

				<listitem>
					<para><emphasis role="strong">XpertDef</emphasis>: module to define brand new
						polymer chemistries; 
					</para>
				</listitem>

				<listitem><para><emphasis role="strong">XpertCalc</emphasis>: module to perform any mass
						calculation taking advantage of defined polymer chemistries;
					</para>
				</listitem>

				<listitem><para><emphasis role="strong">XpertEdit</emphasis>: module to edit polymer
						sequences of any specific polymer chemistry and to graphically perform
						a wide array of chemical reactions along with the corresponding mass
						spectrum simulations; 
					</para>
				</listitem>

				<listitem><para><emphasis role="strong">XpertMiner</emphasis>: module to compare with
						maximum flexibility any number of mass-to-ratio (m/z) lists.
					</para>
				</listitem>

			</itemizedlist>

		</para>
	</refsect1>

	<refsect1>
		<title>OPTIONS</title>
		<variablelist>

			<varlistentry>
				<term><option>? | -h | --help</option></term>
				<listitem> <para>
						Show this help message.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term><option>-c | --config</option></term>
				<listitem>
					<para>
						Display program configuration at build time and the Qt version used to build it.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term><option>-v | --version</option></term>
				<listitem>
					<para>
						Print the version of the software program along the with version of the Qt libraries used to build it.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term><option>-l | --license</option></term>
				<listitem>
					<para>
						Print the license of this software.
					</para>
				</listitem>
			</varlistentry>

			<varlistentry>
				<term><replaceable class="parameter">filename ... </replaceable></term>
				<listitem>
					<para>
						One or more names of files to be opened in the sequence editor.
					</para>
				</listitem>      
			</varlistentry>

		</variablelist>
	</refsect1>

	<refsect1>
		<title>Bibliographic reference to be cited</title>
		<para>
			F. Rusconi (2009) massXpert 2: a cross-platform software environment for
			polymer chemistry modelling and simulation/analysis of mass spectrometric
			data. <emphasis>Bioinformatics</emphasis>, 25:2741-2742.
			doi:10.1093/bioinformatics/btp504.
		</para>
	</refsect1>

		<refsect1>
			<title>See also</title>
			<para>
				<emphasis>minepert (1)</emphasis>
			</para>
		</refsect1>

	<refsect1>
		<title>Useful reading</title>
		<para>
				The program is documented fully in the <emphasis>&progName; User
					Manual</emphasis>, that is packaged in msxpertsuite-doc. That manual is
				available in the form of a PDF-formatted file
				(<filename>/usr/share/doc/msxpertsuite/&progname;-doc.pdf</filename>).
		</para>
	</refsect1>

	<refsect1>
		<title>AUTHOR</title>
		<para>
			<author>
				<firstname>Filippo</firstname>
				<surname>Rusconi &lt;lopippo@debian.org&gt;</surname>
				<authorblurb>
					<para>
						This manual page was written by Filippo Rusconi
						&lt;lopippo@debian.org&gt; (initial writing 05 december
						2013). Permission is granted to copy, distribute and/or
						modify this document under the terms of the GNU General
						Public License, Version 3, published by the Free Software
						Foundation.
					</para>

					<para>
						On a Debian system the complete text of the GNU General
						Public License version 3 can be found in the file
						<filename>/usr/share/common-licenses/GPL\-3</filename>.
					</para>

				</authorblurb>
			</author>
		</para>
	</refsect1>
</refentry>

