Welcome to the msXpertSuite environment for mass spectrometry and mobility ion
spectrometry. This software suite comprises three main software pieces:

- massXpert is a powerful linear polymer chemistry simulation tools that
  allows simulating mass spectra. Bundled with the software are three polymer
  chemistry definition: proteins, nucleic acids and saccharides.

- mineXpert is a powerful mass spectrum viewer with all the features one would
  expect from that kind of software. It also implements nifty mined data
  records storage.

Configuration issues:
=====================

If the massXpert program does not find important data/doc bits, it will produce
a dialog into which the user might enter new locations for the data/doc elements
of the distribution. This dialog can also be shown from the
File->Config settings menu of the massXpert program. Just select the various
locations of the data/doc and the software will be happy again. Note that there
should be no problem when the installed software/data/doc material is not moved
from its original install location.


