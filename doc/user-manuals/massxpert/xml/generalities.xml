<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % msxps-entities SYSTEM "msxpertsuite-user-manuals.ent">
%msxps-entities;

<!ENTITY % massxp-entities SYSTEM "massxpert-entities.ent">
%massxp-entities;

<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">
<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN"
"/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>

<chapter
	xml:id="chap_generalities" 
	xmlns="http://docbook.org/ns/docbook" version="5.0"
	xmlns:xlink="http://www.w3.org/1999/xlink">

	<info>

		<title>Generalities</title>

		<keywordset>
			<keyword>generel concepts</keyword>
			<keyword>combinations</keyword>
			<keyword>integrations</keyword>
		</keywordset>

		<itermset>
			<indexterm zone="chap_generalities"><primary>Integrations</primary></indexterm>
			<indexterm zone="chap_generalities"><primary>Combinations</primary></indexterm>
		</itermset>

	</info>


	<para>In this chapter, I wish to introduce some general concepts around the
		&massXp; program and the way data elements are named in this manual and in
		the program.</para>

	<para> The &massXp; mass spectrometry software suite has been designed to be
		able to <quote>work</quote> with every linear polymer. Well, in a certain
		way this is true&hellip; A more faithful account of the &massXp;'s
		capabilities would be: <quote><emphasis>The &massXp; software suite works
				with whatever polymer chemistry the user cares to define; the more
				accurate the polymer chemistry definition, the more accurate &massXp;
				will be</emphasis></quote>.</para>

	<para> For the program to be able to cope with a variety of possibly very
		different polymers, it had to be written using some <emphasis>abstraction
			layer</emphasis> in between the mass calculations engine and the mere
		description of the polymer sequence. This abstraction layer is implemented
		with the help of <quote>polymer chemistry definitions</quote>, which are
		files describing precisely how a given polymer type should behave in the
		program and what its constitutive entities are. The way polymer chemistry
		definitions are detailed by the user is the subject of a chapter of this
		book (see menu <guimenu>XpertDef</guimenu> of the program).  However, in order
		to give a quick overview, here is a simple situation: a user is working on
		two polymer sequences, one of chemistry type <quote>protein</quote> and
		another one of chemistry type <quote>DNA</quote>.  The protein sequence
		reads <quote>ATGC</quote>, and the DNA sequence reads <quote>CGTA</quote>.
		Now imagine that the user wants to compute the mass of these sequences. How
		will &massXp; know what formula (hence mass) each monomer code corresponds
		to?  There must be a way to inform &massXp; that one of the sequences is a
		protein while the other is a DNA oligonucleotide: this is done upon creation
		of a polymer sequence; the programs asks of what chemistry type the sequence
		to be created is.  Once this <quote>chemical parentage</quote> has been
		defined for each sequence, &massXp; will know how to handle both the
		graphical display of each sequence and the calculations for each
		sequence.</para>


	<sect1 xml:id="sec_on-formulae-and-chemical-reactions">

		<title>On Chemical Formul&aelig; and Chemical Reactions</title>

		<para> Any user of &massXp; will inevitably have to perform two kinds of
			chemical simulations:</para>

		<itemizedlist>

			<listitem>
				<para>
					Define the formula of some chemical entity;
				</para>
			</listitem>

			<listitem>
				<para>
					Define a given chemical reaction, like a protein monomer
					modification, for example.
				</para>
			</listitem>

		</itemizedlist>

		<para> While the definition of a formula poses no special difficulty, the
			definition of a chemical reaction is less trivial, as detailed in the
			following example.  The lysyl residue has the following formula:
			C<subscript>6</subscript>H<subscript>12</subscript>N<subscript>2</subscript>O.
			If that lysyl residue gets acetylated, the acetylation reaction will read
			this way:&mdash;<quote>An acetic acid molecule will condense onto the
				ɛ&nbsp;amine of the lysyl side chain</quote>.  This can also
			read:&mdash;<quote>An acetyl group enters the lysyl side chain while a
				hydrogen atom leaves the lysyl side chain; water is lost in the
				process</quote>. The representation of that reaction is:</para>

		<para> R-NH<subscript>2</subscript> + CH<subscript>3</subscript>COOH
			 ⇋ R-NH-CO-CH<subscript>3</subscript> +
			H<subscript>2</subscript>O</para>

		<para> When the user wants to define that chemical reaction, she can use that
			representation:
			<quote>-H<subscript>2</subscript>O+CH<subscript>3</subscript>COOH</quote>,
			or even the more brief but chemically equivalent one:
			<quote>-H+CH<subscript>3</subscript>CO</quote>. In &massXp;, the chemical
			reaction representation is considered a valid formula.</para>

	</sect1>

	<sect1 xml:id="sec_the-mxp-framework-data-format">

		<title>The &massXp; Framework Data Format</title>

		<para> All the data dealt with in &massXp; are stored on disk as
			XML-formatted files.  XML is the <emphasis>eXtensible Markup
				Language</emphasis>. This <quote>language</quote> allows to describe the
			structure of a document.  The structure of the data is first described in
			a section of the document that is called the <emphasis>Document Type
				Definition, DTD</emphasis>, and the data follow in the same file. One of
			the big advantages of using such XML format in &massXp; is that it is a
			text format, and not a binary one.  This means that any data in the
			&massXp; package is human-readable (even if the XML syntax makes it a bit
			difficult to read data, it is actually possible). Try to read one of the
			polymer chemistry definition XML files that are shipped with this software
			package, and you'll see that these files are pure text files (the same
			applies for the <filename>*.mxp</filename> <acronym>XML</acronym> polymer
			sequence files).  The advantages of using text file formats, with respect
			to binary file formats are:</para>

		<itemizedlist>

			<listitem>
				
				<para>The data in the files are readable even without the program that
					created them. Data extraction is possible, even if it costs work;</para>
		
		</listitem>

		<listitem>
			
			<para>Whenever a text document gets corrupted, it remains possible to
					extract some valid data bits from its uncorrupted parts. With a
					binary format, data are chained from bit to bit; loosing one bit
					lead to automatic corruption of all the remaining bits in the file;</para>
			
			</listitem>

			<listitem>
				
				<para>Text data files are searchable with standard console tools
					(<filename>sed</filename>, <filename>grep</filename>&hellip;), which
					make it possible to search easily text patterns in any text file or
					thousands of these files in one single command line. This is not
					possible with binary format, simply because reading them require the
					program that knows how to decode the data and the powerful
					console-based tools would prove useless.</para>
			
			</listitem>

		</itemizedlist>

	</sect1>

	<sect1 xml:id="sec_chemical-entity-naming-policy">

		<title>Chemical Entity Naming Policy</title> 

		<para> Unless otherwise specified, the user is <emphasis>strongly</emphasis>
			advised <emphasis>not</emphasis> to insert any non-alphanumeric-non-ASCII
			characters (space, %, #, $&hellip;) in the strings that identify polymer
			chemistry definition entities. This means that, for example, users must
			refrain from using non-alphanumeric-non-ASCII characters for the atom
			names and symbols, the names, the codes or the formul&aelig; of the
			monomers or of the modifications, or of the cleavage specifications, or of
			the fragmentation specifications&hellip; Usually, the accepted delimiting
			characters are - and _. It is important not to cripple these polymer
			data for two main reasons: </para>

		<itemizedlist>

			<listitem>

				<para>So that the program performs smoothly (some file-parsing
					processes rely on specific characters (like # or %, for
					example) to isolate sub-strings from larger strings);</para>

			</listitem>

			<listitem>

				<para>So that the results can be easily and clearly displayed when
					time comes to print all the data.</para>

			</listitem>

		</itemizedlist>

	</sect1>

</chapter>

