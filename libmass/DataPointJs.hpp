/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QObject>
#include <QScriptable>
#include <QScriptString>
#include <QScriptClass>


/////////////////////// Local includes
#include <libmass/DataPoint.hpp>
#include <libmass/MassSpectrumJsPrototype.hpp>


namespace msXpSlibmass
{


//! The DataPointJs class provides a JavaScript wrapper around DataPoint.
class DataPointJs : public QObject, public QScriptClass
{
  Q_OBJECT

  friend class MassSpectrumJsPrototype;

  private:
  static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

  static QScriptValue toScriptValue(QScriptEngine *eng,
                                    const DataPoint &dataPoint);
  static void fromScriptValue(const QScriptValue &obj, DataPoint &dataPoint);

  // Both strings need to be initialized with the JS engine.
  QScriptString key;
  QScriptString val;

  QScriptValue proto;
  QScriptValue ctor;

  public:
  DataPointJs(QScriptEngine *engine);
  ~DataPointJs();

  QScriptValue constructor();

  QScriptValue newInstance();
  QScriptValue newInstance(const QString &xyLine);
  QScriptValue newInstance(double mz, double i);
  QScriptValue newInstance(const DataPoint &dataPoint);

  QScriptValue::PropertyFlags propertyFlags(const QScriptValue & /*object*/,
                                            const QScriptString &name,
                                            uint /*id*/);

  QScriptClass::QueryFlags queryProperty(const QScriptValue &object,
                                         const QScriptString &name,
                                         QueryFlags flags,
                                         uint *id);

  QScriptValue
  property(const QScriptValue &object, const QScriptString &name, uint id);

  void setProperty(QScriptValue &object,
                   const QScriptString &name,
                   uint id,
                   const QScriptValue &value);

  QScriptClassPropertyIterator *newIterator(const QScriptValue &object);

  QString name() const;

  QScriptValue prototype() const;
};


} // namespace msXpSlibmass
