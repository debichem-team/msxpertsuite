/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PEAK_SHAPE_HPP
#define PEAK_SHAPE_HPP


/////////////////////// Qt includes
#include <QList>
#include <QPointF>


/////////////////////// Local includes
#include <globals/globals.hpp>
#include <libmass/PeakShapeConfig.hpp>
#include <libmass/PeakCentroid.hpp>


namespace msXpSlibmass
{

// The PeakShape class contains all required data to describe a
// gaussian or lorentzian shape corresponding to a given centroid
// (x,y) with z=m/z and y=relative intensity. The class contains all
// the data rquired to computer either a gaussian shape or a
// lorentzian shape. The points that make the shape are stored as
// QPointF (double x, double y) instances in the
// QList<QPointF*>m_pointList.

class PeakShape : public PeakCentroid
{
  private:
  QList<QPointF *> m_pointList;

  PeakShapeConfig m_config;

  public:
  PeakShape();
  PeakShape(double /* mz */,
            double /* intensity */,
            double /* probability */,
            PeakShapeConfig & /* config */);
  PeakShape(PeakCentroid &, PeakShapeConfig &);

  PeakShape(const PeakShape &);
  ~PeakShape();

  const QList<QPointF *> &pointList() const;

  void appendPoint(QPointF *);
  void emptyPointList();

  QPointF *firstPoint() const;
  QPointF *lastPoint() const;

  QList<QPointF *> *duplicatePointList() const;
  int transferPoints(QList<QPointF *> *);

  const PeakShapeConfig &config() const;
  void setPeakShapeConfig(const PeakShapeConfig &);

  void setMz(double);
  double mz();
  QString mzString();

  void setRelativeIntensity(double);
  double relativeIntensity();
  QString relativeIntensityString();

  int calculatePeakShape();
  int calculateGaussianPeakShape();
  int calculateLorentzianPeakShape();

  double intensityAt(double, double, bool *);

  QString dataAsString();
  bool dataToFile(QString);
};

} // namespace msXpSlibmass

#endif // PEAK_SHAPE_HPP
