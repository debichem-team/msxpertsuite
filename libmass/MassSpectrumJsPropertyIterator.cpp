/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include <QtScript/QScriptEngine>

#include <libmass/MassSpectrumJsPropertyIterator.hpp>
#include <libmass/MassSpectrumJs.hpp>


namespace msXpSlibmass
{


MassSpectrumJsPropertyIterator::MassSpectrumJsPropertyIterator(
  const QScriptValue &object)
  : QScriptClassPropertyIterator(object)
{
  toFront();
}


MassSpectrumJsPropertyIterator::~MassSpectrumJsPropertyIterator()
{
}


bool
MassSpectrumJsPropertyIterator::hasNext() const
{
  MassSpectrum *massSpectrum =
    qscriptvalue_cast<MassSpectrum *>(object().data());
  return m_index < massSpectrum->size();
}


void
MassSpectrumJsPropertyIterator::next()
{
  m_last = m_index;
  ++m_index;
}


bool
MassSpectrumJsPropertyIterator::hasPrevious() const
{
  return (m_index > 0);
}


void
MassSpectrumJsPropertyIterator::previous()
{
  --m_index;
  m_last = m_index;
}


void
MassSpectrumJsPropertyIterator::toFront()
{
  m_index = 0;
  m_last  = -1;
}


void
MassSpectrumJsPropertyIterator::toBack()
{
  MassSpectrum *massSpectrum =
    qscriptvalue_cast<MassSpectrum *>(object().data());
  m_index = massSpectrum->size();
  m_last  = -1;
}


QScriptString
MassSpectrumJsPropertyIterator::name() const
{
  return object().engine()->toStringHandle(QString::number(m_last));
}


uint
MassSpectrumJsPropertyIterator::id() const
{
  return m_last;
}


} // namespace msXpSlibmass
