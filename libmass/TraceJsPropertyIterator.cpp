/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include <QtScript/QScriptEngine>

#include <libmass/TraceJsPropertyIterator.hpp>
#include <libmass/TraceJs.hpp>


namespace msXpSlibmass
{


TraceJsPropertyIterator::TraceJsPropertyIterator(const QScriptValue &object)
  : QScriptClassPropertyIterator(object)
{
  toFront();
}


TraceJsPropertyIterator::~TraceJsPropertyIterator()
{
}


bool
TraceJsPropertyIterator::hasNext() const
{
  Trace *trace = qscriptvalue_cast<Trace *>(object().data());
  return m_index < trace->size();
}


void
TraceJsPropertyIterator::next()
{
  m_last = m_index;
  ++m_index;
}


bool
TraceJsPropertyIterator::hasPrevious() const
{
  return (m_index > 0);
}


void
TraceJsPropertyIterator::previous()
{
  --m_index;
  m_last = m_index;
}


void
TraceJsPropertyIterator::toFront()
{
  m_index = 0;
  m_last  = -1;
}


void
TraceJsPropertyIterator::toBack()
{
  Trace *trace = qscriptvalue_cast<Trace *>(object().data());
  m_index      = trace->size();
  m_last       = -1;
}


QScriptString
TraceJsPropertyIterator::name() const
{
  return object().engine()->toStringHandle(QString::number(m_last));
}


uint
TraceJsPropertyIterator::id() const
{
  return m_last;
}


} // namespace msXpSlibmass
