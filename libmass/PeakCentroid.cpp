/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include <libmass/PeakCentroid.hpp>


namespace msXpSlibmass
{


PeakCentroid::PeakCentroid(double mz, double relIntensity, double probability)
{
  Q_ASSERT(mz >= 0);
  Q_ASSERT(relIntensity >= 0);
  Q_ASSERT(probability >= 0);

  m_mz                = mz;
  m_relativeIntensity = relIntensity;
  m_probability       = probability;
}


PeakCentroid::PeakCentroid(const PeakCentroid &other)
  : m_mz(other.m_mz),
    m_relativeIntensity(other.m_relativeIntensity),
    m_probability(other.m_probability)
{
}


PeakCentroid::~PeakCentroid()
{
}


PeakCentroid *
PeakCentroid::clone() const
{
  PeakCentroid *other = new PeakCentroid(*this);

  return other;
}


void
PeakCentroid::clone(PeakCentroid *other) const
{
  Q_ASSERT(other);

  if(other == this)
    return;

  other->m_mz                = m_mz;
  other->m_relativeIntensity = m_relativeIntensity;
  other->m_probability       = m_probability;
}


void
PeakCentroid::mold(const PeakCentroid &other)
{
  if(&other == this)
    return;

  m_mz                = other.m_mz;
  m_relativeIntensity = other.m_relativeIntensity;
  m_probability       = other.m_probability;
}


PeakCentroid &
PeakCentroid::operator=(const PeakCentroid &other)
{
  if(&other != this)
    mold(other);

  return *this;
}

void
PeakCentroid::setMz(double value)
{
  m_mz = value;
}


double
PeakCentroid::mz() const
{
  return m_mz;
}


void
PeakCentroid::setProbability(double value)
{
  Q_ASSERT(value >= 0);

  m_probability = value;
}


void
PeakCentroid::incrementProbability(double value)
{
  Q_ASSERT(value >= 0);

  m_probability += value;
}


void
PeakCentroid::setRelativeIntensity(double value)
{
  Q_ASSERT(value >= 0);

  m_relativeIntensity = value;
}


double
PeakCentroid::relativeIntensity() const
{
  return m_relativeIntensity;
}


double
PeakCentroid::probability() const
{
  return m_probability;
}


QString
PeakCentroid::probability(int decimalPlaces) const
{
  if(decimalPlaces < 0)
    decimalPlaces = 0;

  QString prob;

  prob.setNum(m_probability, 'f', decimalPlaces);

  return prob;
}


QString
PeakCentroid::relativeIntensity(int decimalPlaces) const
{
  if(decimalPlaces < 0)
    decimalPlaces = 0;

  QString relInt;

  relInt.setNum(m_relativeIntensity, 'f', decimalPlaces);

  return relInt;
}

} // namespace msXpSlibmass
