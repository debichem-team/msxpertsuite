/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PEAK_SHAPE_CONFIG_HPP
#define PEAK_SHAPE_CONFIG_HPP


/////////////////////// Qt includes
#include <QList>
#include <QPointF>


/////////////////////// Local includes
#include <globals/globals.hpp>


namespace msXpSlibmass
{


enum PeakShapeType
{
  PEAK_SHAPE_TYPE_GAUSSIAN   = 1 << 0,
  PEAK_SHAPE_TYPE_LORENTZIAN = 1 << 1,
};

// How many times the whole gaussian/lorentzian curve should span
// around the centroid mz ratio. 4 means that the peak will span
// [ mzRatio - (2 * FWHM) --> mzRatio + (2 * FWHM) ]

extern int FWHM_PEAK_SPAN_FACTOR;


// The PeakShapeConfig class contains all required data to describe a
// gaussian or lorentzian shape corresponding to a given centroid
// (x,y) with z=m/z and y=relative intensity. The class contains all
// the data rquired to computer either a gaussian shape or a
// lorentzian shape. The points that make the shape are stored as
// QPointF (double x, double y) instances in the
// QList<QPointF*>m_pointList.

class PeakShapeConfig
{
  private:
  double m_fwhm;

  int m_pointNumber;
  double m_increment;
  double m_normFactor;
  PeakShapeType m_peakShapeType;

  public:
  PeakShapeConfig();
  PeakShapeConfig(double /* fwhm */,
                  int /* number of points */,
                  float /* increment between two consecutive points */,
                  double /* normFactor */,
                  PeakShapeType /* peakShapeType */);
  PeakShapeConfig(const PeakShapeConfig &);
  ~PeakShapeConfig();

  void setConfig(double /* fwhm */,
                 int /* pointNumber */,
                 float /* increment */,
                 double /*normFactor */,
                 PeakShapeType /* peakShapeType */);

  void setConfig(const PeakShapeConfig &);

  void setFwhm(double);
  double fwhm();
  QString fwhmString();

  void setPointNumber(int);
  int pointNumber();
  QString pointNumberString();

  void setNormFactor(double);
  double normFactor();
  QString normFactorString();

  PeakShapeType peakShapeType();
  void setPeakShapeType(PeakShapeType);

  // For the lorentzian, that is half of the fwhm.
  double hwhm();
  QString hwhmString();

  double a();
  QString aString();

  double c();
  QString cString();

  double gamma();
  QString gammaString();

  void setIncrement(double);
  double increment();
  QString incrementString();

  QString config();
};

} // namespace msXpSlibmass

#endif // PEAK_SHAPE_CONFIG_HPP
