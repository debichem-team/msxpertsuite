/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef ISOTOPIC_PATTERN_CALCULATOR_HPP
#define ISOTOPIC_PATTERN_CALCULATOR_HPP

/////////////////////// Local includes
#include <libmass/PeakCentroid.hpp>
#include <libmass/Formula.hpp>
#include <libmass/PeakShape.hpp>
#include <libmass/PeakShapeConfig.hpp>

namespace msXpSlibmass
{

class IsotopicPatternCalculator : public QObject
{
  Q_OBJECT

  private:
  Formula m_formula;
  int m_charge;

  int m_maxPeakCentroids;
  double m_minProb;

  const QList<Atom *> &m_atomRefList;

  QList<QPointF *> m_pointList;

  PeakShapeConfig m_config;

  QList<PeakCentroid *> m_peakCentroidList;
  QList<PeakCentroid *> m_tempPeakCentroidList;
  QList<PeakShape *> m_peakShapeList;

  bool m_aborted;
  int m_abortCheckCount;
  int m_progressValueNew;
  int m_progressValueOld;

  double m_sumProbabilities;
  double m_greatestProbability;

  bool seedPeakCentroidList();

  void freeClearPeakCentroidList();
  void freeClearTempPeakCentroidList();
  void freeClearPeakShapeList();
  void freeClearPointList();

  int accountAtomCount(const AtomCount *);
  int updatePeakCentroidListWithAtom(const Atom *);
  int updatePeakCentroidWithIsotope(PeakCentroid *, const Isotope *);
  int mergePeakCentroidsWithSameMz();
  int removePeakCentroidsInExcess();
  int calculateSumOfProbabilities();
  int calculateRelativeIntensity();
  int removeTooLowProbPeaks();


  public:
  IsotopicPatternCalculator(Formula /* formula */,
                            int /* charge */,
                            int /* maxPeaks */,
                            double /* minProb */,
                            const QList<Atom *> & /* atomRefList */,
                            PeakShapeConfig = PeakShapeConfig());

  ~IsotopicPatternCalculator();

  const QList<PeakCentroid *> &peakCentroidList() const;
  QString *peakCentroidListAsString() const;

  const QList<QPointF *> &pointList() const;
  QString *pointListAsString() const;
  QList<QPointF *> *duplicatePointList() const;
  int transferPoints(QList<QPointF *> *);

  QPointF *firstPoint() const;
  QPointF *lastPoint() const;

  int calculatePeakCentroids();
  int calculatePeakShapes();
  const QList<QPointF *> &sumPeakShapes();


  signals:
  void isotopicCalculationProgressValueChanged(int);
  void isotopicCalculationMessageChanged(QString);

  public slots:
  void isotopicCalculationAborted();
};

} // namespace msXpSlibmass

#endif // ISOTOPIC_PATTERN_CALCULATOR_HPP
