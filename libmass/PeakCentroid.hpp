/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PEAK_CENTROID_HPP
#define PEAK_CENTROID_HPP


/////////////////////// Qt includes
#include <QtGlobal>
#include <QtXml>


/////////////////////// Local includes


namespace msXpSlibmass
{

class PeakCentroid
{
  protected:
  // m/z ratio.
  double m_mz;

  // Relative intensity of this peak with respect to all the others
  // in the pattern.
  double m_relativeIntensity;

  // Probability of occurrence.
  double m_probability;

  public:
  PeakCentroid(double /* mz */,
               double /* relIntensity */,
               double /* probability */);
  PeakCentroid(const PeakCentroid &);

  ~PeakCentroid();

  PeakCentroid *clone() const;
  void clone(PeakCentroid *) const;
  void mold(const PeakCentroid &);
  PeakCentroid &operator=(const PeakCentroid &);

  void setMz(double);
  double mz() const;

  void setProbability(double);
  void incrementProbability(double);
  double probability() const;
  QString probability(int decimalPlaces) const;

  void setRelativeIntensity(double);
  double relativeIntensity() const;
  QString relativeIntensity(int decimalPlaces) const;
};

} // namespace msXpSlibmass


#endif // PEAK_CENTROID_HPP
