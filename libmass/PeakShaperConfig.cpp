/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include <libmass/PeakShaperConfig.hpp>

// For the M_PI Pi value
#include <qmath.h>


namespace msXpSlibmass
{


PeakShaperConfig::PeakShaperConfig()
  : m_resolution{0},
    m_fwhm{0},
    m_ionizationFormula{QString()},
    m_charge{1},
    m_pointCount{0},
    m_mzStep{0},
    m_normFactor{1},
    m_peakShapeType{PeakShapeType::GAUSSIAN}
{
}


PeakShaperConfig::PeakShaperConfig(int resolution,
                                   double fwhm,
                                   const QString &ionization_formula,
                                   int charge,
                                   int pointCount,
                                   double mzStep,
                                   double normFactor,
                                   PeakShapeType peakShapeType)
  : m_resolution{resolution},
    m_fwhm{fwhm},
    m_ionizationFormula{ionization_formula},
    m_charge{charge},
    m_pointCount{pointCount},
    m_mzStep{mzStep},
    m_normFactor{normFactor},
    m_peakShapeType{peakShapeType}
{
}


PeakShaperConfig::PeakShaperConfig(const PeakShaperConfig &other)
  : m_resolution{other.m_resolution},
    m_fwhm{other.m_fwhm},
    m_ionizationFormula{other.m_ionizationFormula},
    m_charge{other.m_charge},
    m_pointCount{other.m_pointCount},
    m_mzStep{other.m_mzStep},
    m_normFactor{other.m_normFactor},
    m_peakShapeType{other.m_peakShapeType}
{
}


void
PeakShaperConfig::operator=(const PeakShaperConfig &other)
{
  m_resolution        = other.m_resolution;
  m_fwhm              = other.m_fwhm;
  m_ionizationFormula = other.m_ionizationFormula;
  m_charge            = other.m_charge;
  m_pointCount        = other.m_pointCount;
  m_mzStep            = other.m_mzStep;
  m_normFactor        = other.m_normFactor;
  m_peakShapeType     = other.m_peakShapeType;
}


PeakShaperConfig::~PeakShaperConfig()
{
}


void
PeakShaperConfig::setConfig(int resolution,
                            double fwhm,
                            const QString &ionization_formula,
                            int charge,
                            int pointCount,
                            double mzStep,
                            double normFactor,
                            PeakShapeType peakShapeType)
{
  m_resolution        = resolution;
  m_fwhm              = fwhm;
  m_ionizationFormula = ionization_formula;
  m_charge            = charge;
  m_pointCount        = pointCount;
  m_mzStep            = mzStep;
  m_normFactor        = normFactor;
  m_peakShapeType     = peakShapeType;
}


void
PeakShaperConfig::setConfig(const PeakShaperConfig &other)
{
  m_resolution        = other.m_resolution;
  m_fwhm              = other.m_fwhm;
  m_ionizationFormula = other.m_ionizationFormula;
  m_charge            = other.m_charge;
  m_pointCount        = other.m_pointCount;
  m_mzStep            = other.m_mzStep;
  m_normFactor        = other.m_normFactor;
  m_peakShapeType     = other.m_peakShapeType;
}


void
PeakShaperConfig::setResolution(int value)
{
  m_resolution = value;

  if(!m_resolution)
    {
      // We do not change m_fwhm that might have been set to a proper value.
    }
  else
    {
      m_fwhm = 0;
    }
}


int
PeakShaperConfig::resolution(double mz)
{
  if(m_resolution)
    return m_resolution;

  if(!m_fwhm)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "FWHM cannot be 0 as it is the denominator."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  m_resolution = mz / m_fwhm;

  return m_resolution;
}


void
PeakShaperConfig::setFwhm(double value)
{
  m_fwhm = value;

  if(!m_fwhm)
    {
      // We do not change m_resolution that might have been set to a proper
      // value.
    }
  else
    {
      m_resolution = 0;
    }
}


double
PeakShaperConfig::fwhm(double mz)
{
  if(m_fwhm)
    return m_fwhm;

  if(!m_resolution)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Resolution cannot be 0 as it is the denominator."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  m_fwhm = mz / m_resolution;

  return m_fwhm;
}


QString
PeakShaperConfig::fwhmAsText(double mz)
{
  return QString("%1").arg(fwhm(mz), 0, 'd');
}


QString
PeakShaperConfig::resolutionAsText() const
{
  QString string;
  string.setNum(m_resolution);

  return string;
}

// For the lorentzian, that is half of the fwhm.
double
PeakShaperConfig::halfFwhm(double mz)
{
  return (fwhm(mz) / 2);
}


// For the lorentzian, that is half of the fwhm.
QString
PeakShaperConfig::halfFwhmAsText(double mz)
{
  QString string;
  string.setNum(fwhm(mz) / 2);

  return string;
}

void
PeakShaperConfig::setIonizationFormula(const QString &ionization_formula)
{
  m_ionizationFormula = ionization_formula;
}


QString
PeakShaperConfig::getIonizationFormula() const
{
  return m_ionizationFormula;
}


void
PeakShaperConfig::setCharge(int value)
{
  m_charge = value;
}


int
PeakShaperConfig::getCharge() const
{
  return m_charge;
}


QString
PeakShaperConfig::chargeAsText() const
{
  QString string;
  string.setNum(m_charge);

  return string;
}


void
PeakShaperConfig::setPointCount(int value)
{
  m_pointCount = value;
}


int
PeakShaperConfig::getPointCount() const
{
  return m_pointCount;
}


QString
PeakShaperConfig::pointCountAsText() const
{
  QString string;
  string.setNum(m_pointCount);

  return string;
}


void
PeakShaperConfig::setNormFactor(double value)
{
  m_normFactor = value;
}


double
PeakShaperConfig::normFactor()
{
  return m_normFactor;
}


QString
PeakShaperConfig::normFactorAsText()
{
  QString string;
  string.setNum(m_normFactor);

  return string;
}

void
PeakShaperConfig::setPeakShapeType(PeakShapeType value)
{
  m_peakShapeType = value;
}


PeakShapeType
PeakShaperConfig::peakShapeType()
{
  return m_peakShapeType;
}


double
PeakShaperConfig::c(double mz)
{
  double c = fwhm(mz) / (2 * sqrt(2 * log(2)));

  // qDebug() << __FILE__ << __LINE__
  //          << "c:" << c;

  return c;
}

QString
PeakShaperConfig::cAsText(double mz)
{

  double cValue = c(mz);

  QString string;
  string.setNum(cValue);

  return string;
}


double
PeakShaperConfig::a(double mz)
{
  //  double pi = 3.1415926535897932384626433832795029;

  double a = (1 / (c(mz) * sqrt(2 * M_PI)));

  // qDebug() << __FILE__ << __LINE__
  //          << "a:" << a;

  return a;
}


QString
PeakShaperConfig::aAsText(double mz)
{
  double aValue = a(mz);

  QString string;
  string.setNum(aValue);

  return string;
}


double
PeakShaperConfig::gamma(double mz)
{
  double gamma = fwhm(mz) / 2;

  // qDebug() << __FILE__ << __LINE__
  //          << "gamma:" << gamma;

  return gamma;
}

QString
PeakShaperConfig::gammaAsText(double mz)
{
  double gammaValue = gamma(mz);

  QString string;
  string.setNum(gammaValue);

  return string;
}


void
PeakShaperConfig::setMzStep(double value)
{
  m_mzStep = value;
}


double
PeakShaperConfig::mzStep(double mz)
{

  // But what is the mz step ?
  //
  // We want the shape to be able to go down to baseline. Thus we want that the
  // shape to have a "basis" (or, better, a "ground") corresponding to twice the
  // FWHM on the left of the centroid and to twice the FWHM on the right (that
  // makes in total FWHM_PEAK_SPAN_FACTOR * FWHM).

  if(m_mzStep)
    return m_mzStep;

  if(!m_pointCount)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Point count cannot be 0 as it is the denominator."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  m_mzStep = (msXpS::FWHM_PEAK_SPAN_FACTOR * fwhm(mz)) / m_pointCount;

  return m_mzStep;
}


QString
PeakShaperConfig::mzStepAsText(double mz)
{
  QString string;
  string.setNum(m_mzStep);

  return string;
}


QString
PeakShaperConfig::asText(double mz)
{
  QString string;

  if(m_peakShapeType == msXpSlibmass::PeakShapeType::GAUSSIAN)
    {

      string = QString(
                 "Gaussian peak shaping:\n"
                 "Configuration for m/z value: %1\n"
                 "Resolution: %2\n"
                 "FWHM: %3\n"
                 "Number of points to shape the peak: %4\n"
                 "c: %5\n"
                 "c^2: %6\n"
                 "mz step: %7\n\n")
                 .arg(mz, 0, 'f', 5)
                 .arg(resolution(mz))
                 .arg(fwhm(mz), 0, 'f', 5)
                 .arg(m_pointCount)
                 .arg(c(mz), 0, 'f', 5)
                 .arg(c(mz) * c(mz), 0, 'f', 5)
                 .arg(mzStep(mz), 0, 'f', 5);
    }

  if(m_peakShapeType == msXpSlibmass::PeakShapeType::LORENTZIAN)
    {
      string = QString(
                 "Lorentzian peak shaping:\n"
                 "Configuration for m/z value: %1\n"
                 "Resolution: %2\n"
                 "FWHM: %3\n"
                 "Number of points to shape the peak: %4\n"
                 "gamma: %5\n"
                 "gamma^2: %6\n"
                 "mz step: %7\n\n")
                 .arg(mz, 0, 'f', 5)
                 .arg(resolution(mz))
                 .arg(fwhm(mz), 0, 'f', 5)
                 .arg(m_pointCount)
                 .arg(gamma(mz), 0, 'f', 5)
                 .arg(gamma(mz) * gamma(mz), 0, 'f', 5)
                 .arg(mzStep(mz), 0, 'f', 5);
    }

  return string;
}


} // namespace msXpSlibmass
