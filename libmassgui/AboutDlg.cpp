/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QGraphicsPixmapItem>
#include <QFile>
#include <QDebug>


/////////////////////// Local includes
#include <libmassgui/AboutDlg.hpp>
#include <config.h>
#include <globals/About_gpl_v30.hpp>
#include <libmassgui/About_history.hpp>


namespace msXpSlibmassgui
{

AboutDlg::AboutDlg(QWidget *parent, const QString &applicationName)
  : QDialog{parent}, m_applicationName{applicationName}
{
  if(parent == nullptr)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  m_ui.setupUi(this);

  QString text = QString("msXpertSuite - module %1, version %2.")
                   .arg(m_applicationName)
                   .arg(VERSION);

  m_ui.programVersionLabel->setText(text);

  setWindowTitle(QString("%1 - About").arg(m_applicationName));

  connect(m_ui.closePushButton, SIGNAL(clicked()), this, SLOT(accept()));


  setupGraphicsView();

  m_ui.copyrightLabel->setText(
    "msXpertSuite Copyright 2016 by Filippo Rusconi");
  m_ui.gplTextEdit->setText(msXpSlibmass::gpl);
}


AboutDlg::~AboutDlg()
{
}


void
AboutDlg::setupGraphicsView()
{
  mp_graphicsScene = new QGraphicsScene();

  QPixmap pixmap(":/images/splashscreen.png");

  QGraphicsPixmapItem *pixmapItem = mp_graphicsScene->addPixmap(pixmap);
  if(!pixmapItem)
    return;
  m_ui.graphicsView->setScene(mp_graphicsScene);
}


void
AboutDlg::showHowToCiteTab()
{
  m_ui.tabWidget->setCurrentIndex(1);
}


} // namespace msXpSlibmassgui
