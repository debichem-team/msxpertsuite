/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef ABOUT_DLG_HPP
#define ABOUT_DLG_HPP


/////////////////////// Qt includes
#include <QGraphicsScene>


/////////////////////// Local includes
#include "ui_AboutDlg.h"


namespace msXpSlibmassgui
{

class AboutDlg : public QDialog
{
  Q_OBJECT

  protected:
  Ui::AboutDlg m_ui;

  QString m_applicationName;

  // Allocated but ownership taken by the graphicsView.
  QGraphicsScene *mp_graphicsScene;


  public:
  AboutDlg(QWidget *, const QString &applicationName);

  virtual ~AboutDlg();

  virtual void setupGraphicsView();
  virtual void setHistoryText() = 0;

  virtual void showHowToCiteTab();

  // Make this function pure virtual so that the
  // user of the class needs to implement it with
  // a text that describes specifically the features of that peculiar
  // application.
  virtual void setApplicationFeaturesText() = 0;
};

} // namespace msXpSlibmassgui


#endif // ABOUT_DLG_HPP
