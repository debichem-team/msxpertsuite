/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QFile>
#include <QDebug>


/////////////////////// Local includes
#include <massxpert/nongui/MonomerSpec.hpp>


namespace msXpSmassXpert
{

void
MonomerSpec::setName(const QString &name)
{
  m_name = name;
}


const QString &
MonomerSpec::name()
{
  return m_name;
}

void
MonomerSpec::setCode(const QString &code)
{
  m_code = code;
}


const QString &
MonomerSpec::code()
{
  return m_code;
}


void
MonomerSpec::setRaster(const QString &raster)
{
  m_raster = raster;
}


const QString &
MonomerSpec::raster()
{
  return m_raster;
}


void
MonomerSpec::setVector(const QString &vector)
{
  m_vector = vector;
}


const QString &
MonomerSpec::vector()
{
  return m_vector;
}


void
MonomerSpec::setNameSound(const QString &sound)
{
  m_nameSound = sound;
}


const QString &
MonomerSpec::nameSound()
{
  return m_nameSound;
}


void
MonomerSpec::setCodeSound(const QString &sound)
{
  m_codeSound = sound;
}


const QString &
MonomerSpec::codeSound()
{
  return m_codeSound;
}


bool
MonomerSpec::parseFile(QString &filePath, QList<MonomerSpec *> *specList)
{
  MonomerSpec *monomerSpec = 0;

  qint64 lineLength;

  QString line;
  QString temp;

  char buffer[1024];

  int percentSignIdx = 0;
  int pipeSignIdx    = 0;

  Q_ASSERT(specList != 0);

  if(filePath.isEmpty())
    return false;

  QFile file(filePath);

  if(!file.open(QFile::ReadOnly))
    return false;

  // The lines we have to parse are of the following type:
  // A%alanine.svg|alanine.png
  // Any line starting with ' ' or '#' are not parsed.

  // Get the first line of the file. Next we enter in to a
  // while loop.

  lineLength = file.readLine(buffer, sizeof(buffer));

  while(lineLength != -1)
    {
      // The line is now in buffer, and we want to convert
      // it to Unicode by setting it in a QString.
      line = buffer;

      // The line that is in line should contain something like:
      // A%alanine.svg|alanine.png
      //
      // Note, however that lines beginning with either ' '(space) or
      // '\n'(newline) or '#' are comments.

      // Remove all the spaces from the borders: Whitespace means any
      // character for which QChar::isSpace() returns true. This
      // includes the ASCII characters '\t', '\n', '\v', '\f', '\r',
      // and ' '.

      line = line.trimmed();

      if(line.isEmpty() || line.startsWith('#', Qt::CaseInsensitive))
        {
          lineLength = file.readLine(buffer, sizeof(buffer));
          continue;
        }

      // Now some other checks. Remember the format of the line:
      // A%alanine.svg|alanine.png

      percentSignIdx = line.indexOf('%', 0, Qt::CaseInsensitive);
      pipeSignIdx    = line.indexOf('|', 0, Qt::CaseInsensitive);

      if(percentSignIdx == -1 || line.count('%', Qt::CaseInsensitive) > 1 ||
         line.count('|', Qt::CaseInsensitive) > 1)
        return false;

      if(pipeSignIdx != -1)
        if(percentSignIdx > pipeSignIdx)
          return false;

      // OK, we finally can allocate a new MonomerSpec *.
      monomerSpec = new MonomerSpec();

      monomerSpec->m_code = line.left(percentSignIdx);

      //       qDebug() << __FILE__ << __LINE__
      // 		<< "m_code" << monomerSpec->m_code.toAscii();

      // Remove from the line the "A%" substring, as we do not need it
      // anymore.
      line.remove(0, percentSignIdx + 1);

      if(pipeSignIdx == -1)
        {
          // If there was no pipe sign in the line, we have a single
          // substring up to the end of the line.

          if(line.endsWith(".png", Qt::CaseSensitive))
            monomerSpec->m_raster = line;
          else if(line.endsWith(".svg", Qt::CaseSensitive))
            monomerSpec->m_vector = line;
          else
            {
              delete monomerSpec;
              return false;
            }
        }
      else
        {
          // There was a pipe sign indeed, we have to divide the
          // remaining substring into two parts, before and after the
          // '|'.

          // Extract the left substring.
          temp = line.section('|', 0, 0);
          if(temp.endsWith(".png", Qt::CaseSensitive))
            monomerSpec->m_raster = temp;
          else if(temp.endsWith(".svg", Qt::CaseSensitive))
            monomerSpec->m_vector = temp;
          else
            {
              delete monomerSpec;
              return false;
            }

          // Extract the right substring.
          temp = line.section('|', 1, 1);
          if(temp.endsWith(".png", Qt::CaseSensitive))
            monomerSpec->m_raster = temp;
          else if(temp.endsWith(".svg", Qt::CaseSensitive))
            monomerSpec->m_vector = temp;
          else
            {
              delete monomerSpec;
              return false;
            }
        }

      specList->append(monomerSpec);
      lineLength = file.readLine(buffer, sizeof(buffer));
    }

  return true;
}

} // namespace msXpSmassXpert
