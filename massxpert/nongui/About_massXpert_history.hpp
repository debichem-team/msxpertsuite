/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


namespace msXpSmassXpert
{

QString history(
  "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
  "<html>\n"
  "<head>\n"
  "  <meta content=\"text/html; charset=ISO-8859-1\" "
  "http-equiv=\"content-type\">\n"
  "  <title>massxpert-history</title>\n"
  "</head>\n"
  "<body>\n"
  "<div style=\"text-align: center;\">This is a brief history of <span "
  "style=\"font-style: italic;\">massXpert</span><br>\n"
  "</div>\n"
  "<br>\n"
  "<span style=\"font-weight: bold;\">1998-2000</span><br>\n"
  "<br>\n"
  "The name massXpert comes from a project I started while I was a\n"
  "post-doctoral fellow of the Ecole Polytechnique at the Institut\n"
  "Europ&eacute;en de Chimie et Biologie, Pessac(Bordeaux), France.<br>\n"
  "<br>\n"
  "The <span style=\"font-style: italic;\">massXpert</span> program was "
  "published in <span style=\"font-style: "
  "italic;\">Bioinformatics</span>(<strong>Rusconi, F.</strong> and "
  "Belghazi, M. Desktop prediction/analysis of mass spectrometric data in "
  "proteomic projects by using massXpert. <em></em><em>Bioinformatics</em> "
  "2002 18(4):644-5).<br>\n"
  "<br>\n"
  "At that time, MS-Windows was at the Windows NT 4.0 version and the next\n"
  "big release was going to be \"you'll see what you'll see\" : MS-Windows\n"
  "2000.<br>\n"
  "<br>\n"
  "When I tried massXpert on that new version(one colleague had it with a\n"
  "new machine), I discovered that my software would not run normally(the\n"
  "editor was broken). The Microsofties would advise to \"buy a new version\n"
  "of the compiler environment and rebuild\". Noooo! I did not want to\n"
  "continue paying for <span style=\"font-weight: bold;\">only using</span> "
  "something I had produced !<br>\n"
  "<br>\n"
  "<span style=\"font-weight: bold;\">2001-2006</span><br>\n"
  "<br>\n"
  "I decided during fall 1999 that I would stop using Microsoft products\n"
  "for my development. At the beginning of 2000 I started as a CNRS research "
  "staff in a new laboratory and decided to start fresh: I switched to "
  "GNU/Linux(I never looked back). After some months of learning, I felt "
  "mature to start a new development project that would eventually\n"
  "become an official GNU package: <span style=\"font-style: italic;\">GNU "
  "polyxmass</span>.<br>\n"
  "<br>\n"
  "The GNU polyxmass software, much more powerful than massXpert was, was "
  "published in <span style=\"font-style: italic;\">BMC "
  "Bioinformatics</span> in 2006(<strong>Rusconi, F.</strong>, GNU "
  "polyxmass: a software framework for mass spectrometric simulations of "
  "linear(bio-)polymeric analytes. <em>BMC Bioinformatics</em> 2006, 7:226; "
  "published 27 April 2006).<br>\n"
  "<br>\n"
  "Following that publication I got a lot of feedback(very positive, in a\n"
  "way) along the lines: \"Hey, your software looks very interesting; it's\n"
  "only a pity we cannot use it because it runs on GNU/Linux, and we only\n"
  "use MS-Windows and MacOSX!\".<br>\n"
  "<br>\n"
  "<span style=\"font-weight: bold;\">2007-</span><br>\n"
  "<br>\n"
  "I decided to make a full rewrite of GNU polyxmass and the software that\n"
  "you are running now is the product of that rewrite. I decided to\n"
  "\"recycle\" the massXpert name because this soft is written in C++, as\n"
  "was its ancestor. Also, because the first MS-Windows-based massXpert\n"
  "project is not developed anymore, taking that name was kind of a\n"
  "\"revival\" which I enjoyed. However, the toolkit I used this time is\n"
  "not the&nbsp;Microsoft Foundation Classes(first massXpert version) but "
  "the\n"
  "Trolltech Qt framework(this software, see the \"About Qt\" help menu). "
  "<br>\n"
  "<br>\n"
  "Coding with Qt has one big advantage: it allows the developer to code\n"
  "once and to compile on the three main platforms available today:\n"
  "GNU/Linux, MacOSX, MS-Windows. Another advantage is that Qt is\n"
  "wonderful software(Free Software).<br>\n"
  "<br>\n"
  "Enjoy massXpert !<br>\n"
  "<br>\n"
  "Filippo Rusconi,<br>\n"
  "author of massXpert<br><br>\n"
  "<br><br>\n"
  "</body>\n"
  "</html>\n");

} // namespace msXpSmassXpert
