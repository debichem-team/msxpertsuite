/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


////////////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QTextStream>


/*
 Bitwise stuff (from StackOverflow)

 It is sometimes worth using an enum to name the bits:

 enum ThingFlags = {
 ThingMask  = 0x0000,
 ThingFlag0 = 1 << 0,
 ThingFlag1 = 1 << 1,
 ThingError = 1 << 8,
 }

 Then use the names later on. I.e. write

 thingstate |= ThingFlag1;
 thingstate &= ~ThingFlag0;
 if (thing & ThingError) {...}

 to set, clear and test. This way you hide the magic numbers from the rest of
 your code.
 */


namespace msXpSmassXpert
{

//! End of the polymer sequence.
enum PolymerEnd
{
  END_NONE  = 0 << 1,
  END_LEFT  = 1 << 1,
  END_RIGHT = 2 << 1,
  END_BOTH  = (END_LEFT | END_RIGHT),
};


//! Type of capping.
/*! The sequence may be capped at left end , at right end, at both
 *  ends or not capped at all.
 */
enum CapType
{
  CAP_NONE  = 0 << 1,
  CAP_LEFT  = 1 << 1,
  CAP_RIGHT = 2 << 1,
  CAP_BOTH  = (CAP_LEFT | CAP_RIGHT),
};


//! Monomer chemical entities.
/*! The monomer chemical entities can be nothing or modifications.
 */
enum MonomerChemEnt
{
  MONOMER_CHEMENT_NONE       = 0 << 1,
  MONOMER_CHEMENT_MODIF      = 1 << 1,
  MONOMER_CHEMENT_CROSS_LINK = 1 << 2,
};


//! Polymer chemical entities.
/*! The polymer chemical entities can be nothing or left end
 *  modification or right end modification or both ends modifications.
 */
enum PolymerChemEnt
{
  POLYMER_CHEMENT_NONE                  = 1 << 0,
  POLYMER_CHEMENT_LEFT_END_MODIF        = 1 << 1,
  POLYMER_CHEMENT_FORCE_LEFT_END_MODIF  = 1 << 2,
  POLYMER_CHEMENT_RIGHT_END_MODIF       = 1 << 3,
  POLYMER_CHEMENT_FORCE_RIGHT_END_MODIF = 1 << 4,
  POLYMER_CHEMENT_BOTH_END_MODIF =
    (POLYMER_CHEMENT_LEFT_END_MODIF | POLYMER_CHEMENT_RIGHT_END_MODIF),
  POLYMER_CHEMENT_VORCE_BOTH_END_MODIF = (POLYMER_CHEMENT_FORCE_LEFT_END_MODIF |
                                          POLYMER_CHEMENT_FORCE_RIGHT_END_MODIF)
};


enum HashAccountData
{
  HASH_ACCOUNT_SEQUENCE      = 1 << 0,
  HASH_ACCOUNT_MONOMER_MODIF = 1 << 1,
  HASH_ACCOUNT_POLYMER_MODIF = 1 << 2,
};


} // namespace msXpSmassXpert
