/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CALCULATOR_WND_HPP
#define CALCULATOR_WND_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include <ui_CalculatorWnd.h>
#include <massxpert/gui/AbstractMainTaskWindow.hpp>
#include <libmass/Ponderable.hpp>
#include <massxpert/nongui/PolChemDef.hpp>
#include <massxpert/gui/CalculatorRecorderDlg.hpp>
#include <massxpert/gui/CalculatorChemPadDlg.hpp>


namespace msXpSmassXpert
{

enum MxpFormulaHandling
{
  FORMULA_HANDLING_IMMEDIATE  = 1 << 0,
  FORMULA_HANDLING_PRINT      = 1 << 1,
  FORMULA_HANDLING_WITH_SPACE = 1 << 2,
  FORMULA_HANDLING_PRINT_WITH_SPACE =
    (FORMULA_HANDLING_PRINT | FORMULA_HANDLING_WITH_SPACE),
};


class CalculatorWnd : public AbstractMainTaskWindow
{
  Q_OBJECT

  private:
  Ui::CalculatorWnd m_ui;

  msXpSlibmass::Ponderable m_seedPonderable;
  msXpSlibmass::Ponderable m_tempPonderable;
  msXpSlibmass::Ponderable m_resultPonderable;

  CalculatorRecorderDlg *mpa_recorderDlg = 0;
  CalculatorChemPadDlg *mpa_chemPadDlg   = 0;

  PolChemDef m_polChemDef;

  void readSettings();
  void writeSettings();

  bool initialize();

  void closeEvent(QCloseEvent *event);

  public:
  CalculatorWnd(MainWindow *parent,
                const QString &polChemDefFilePath,
                const QString &mono = QString(),
                const QString &avg  = QString());

  ~CalculatorWnd();

  bool m_forciblyClose  = false;
  int m_formulaHandling = FORMULA_HANDLING_IMMEDIATE;


  const PolChemDef &polChemDef();
  QString polChemDefName();

  void updateWindowTitle();
  bool populatePolChemDefComboBoxes();

  bool setupChemicalPad();

  void recordResult();
  void updateSeedResultLineEdits();

  void recorderDlgClosed();
  void chemPadDlgClosed();

  int accountFormula(const QString & = QString(""), int = 1);
  int accountMonomer();
  int accountModif();
  int accountSequence();

  void setFormulaHandling(int);

  void addFormulaToMemory();
  void removeFormulaFromMemory();
  void clearWholeMemory();
  QString simplifyFormula();

  public slots:
  void addToResult();
  void sendToResult();
  void removeFromResult();
  void clearSeed();
  void addToSeed();
  void sendToSeed();
  void removeFromSeed();
  void clearResult();
  void showRecorder(int);

  void formulaActionsComboBoxActivated(int);

  void showChemPad(int);

  void apply(const QString & = QString(""));

  void mzCalculation();
  void isotopicPatternCalculation();
};

} // namespace msXpSmassXpert


#endif // CALCULATOR_WND_HPP
